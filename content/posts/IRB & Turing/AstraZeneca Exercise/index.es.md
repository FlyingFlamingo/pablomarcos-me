---
title: "AstraZeneca Excersise"
author: Pablo Marcos
date: 2023-10-24
math: true
menu:
  sidebar:
    name: AstraZeneca Exercise
    identifier: astrazeneca_exercise
    parent: irb_turing_more
    weight: 40
---

The first place I searched for a job in was [AstraZeneca](https://www.astrazeneca.fr/), as I was really atracted by their position as a consolidated & international pharma company. After a really hard interview for a Knowledge Graph engineer role, I was presented with a challenge, which consisted on making the following code work:

```python
class Node:
    def __init__(self, node_id):
        self.id = node_id
        self.parents = set()
        self.children = set()

        # I first solved it using all_parents as a property, but
        # I thought this made less sense in the end
        # self.all_parents = set()

    def __repr__(self):
        return str(self.id)

    def add_child(self, node):
        node.parents.add(self)
        self.children.add(node)

    def get_all_parents(self):

        all_parents = set()

        for each_node in self.parents:
            node_parents = each_node.get_all_parents()

            # For the original node, all their parents will be:
            all_parents.add(each_node) # His own parents

            # And all the parents of their parents
            all_parents.update(node_parents)


        return all_parents # And I return all the parents
```

```python
class Graph:
    def __init__(self):
        self.nodes = {}

    def add_node(self, node_id):
        if node_id not in self.nodes:
            self.nodes[node_id] = Node(node_id)
        return self.nodes.get(node_id)

    def add_edge(self, parent_id, child_id):
        parent_node = self.add_node(parent_id)
        child_node = self.add_node(child_id)
        parent_node.add_child(child_node)

    def find_all_parents(self, node_id):

        # If the node id exists
        if node_id in self.nodes:

            # Get node by id
            this_node = self.nodes.get(node_id)

            # And query for all their parents
            all_the_found_parents = this_node.get_all_parents()

            if any(all_the_found_parents):
                return all_the_found_parents
            else:
                return None

        # If it doesn't, raise a KeyError
        else:
            raise KeyError("Node does not exist on DB")
```

```python
if __name__ == '__main__':
    # Example usage:
    g = Graph()
    g.add_edge(1, 2)
    g.add_edge(1, 3)
    g.add_edge(2, 4)
    g.add_edge(3, 4)
    g.add_edge(5, 3)
    g.add_edge(4, 6)

    print(g.find_all_parents(4))
    # Output: {2, 5, 3, 1}

    print(g.find_all_parents(6))
    # Output: {2, 5, 3, 1, 4}

    print(g.find_all_parents(5))
    # Output: None

    print(g.find_all_parents(3))
    # Output: {1, 5}

    print(g.find_all_parents(42))
    # Output: KeyError
```

I liked my solution, and I wanted to share it here :p

Hope to find something soon!

📸 : [Braño](https://unsplash.com/@3dparadise) on [Unsplash](https://unsplash.com/photos/3-clear-glass-bottles-on-table-QSuou3VAtf4)

