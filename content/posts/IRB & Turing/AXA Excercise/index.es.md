---
title: "AXA Excersise"
author: Pablo Marcos
date: 2024-12-12
math: true
menu:
  sidebar:
    name: Axa Exercise
    identifier: axa_exercise
    parent: irb_turing_more
    weight: 40
---

{{< badge href="./axa_geo_exercise.ipynb" badge="jupyter" >}} </br>

During the process of a candidature for a Bioinformatics position at [AXA](https://biomemakers.com/es/inicio), I was assigned the following exercise. In keeping with the spirit of this blog, I have only released my solution, and not the assignment, in order to both protect AXA's copyright and share my proposed solution to an interesting challenge.

{{< alert type="success" >}} I finally ended up working here! :p {{< /alert >}}

---

# Analyzing Sample Locations & Geocoders

We have been given a dummy dataset with 100 sites that have been geocoded using the following geocoders: **apple, straw, grapes & orange**. In this dataset, the `v_lat` and `v_lon` columns represent the real location of the samples, that has been manually verified; whereas, the geocoders implement their own location protocols. Based on this dataset, we should:

1. Analyze which country the sites are located in.
2. Give a reasoned guess as to which geocoder is the best, how they rank between each other, and according to which criteria.

To do so, I have prepared the following notebook.

## Summary

1. Samples are all located inside the United States of America, **according to OSM and GADM**
2. Plotting them on a leaflet map confirms visually that their locations are **in the United States**
3. There is one sample over water; it should be investigated whether its location is correct
4. The geocoders can be ranked **based on their average distance to ground truth** as follows: **apple > straw >> grapes >>> orange**
5. **apple** and **straw** rank closely; however, **apple** has an smaller distance to ground truth, less outliers, and smaller quartile limits; thus **apple is the best geocoder**.


## Finding out the country location for our samples

There are a lot of different ways we could find the location of our samples. For simplicity, I will implement two: an API call to OpenStreetMap Nominatim, based on my work at my own [mapa-vxl project](https://codeberg.org/FlyingFlamingo/mapa-vxl); and a direct spatial join using data from [the GADM project](https://gadm.org/).

For the Nominatim part, it is easy: we just need to define a function that queries OSM using python's `geopy` library, and then call it:


```python
import pandas as pd    # Work with CSVs

# Read the CSV file, and re-name the columns to more human-friendly names:
points_df = pd.read_csv("interview_geocode.csv", usecols=lambda column: column != "Unnamed: 0")
points_df = points_df.rename(columns={'v_lat': 'real_latitude', 'v_lon': 'real_longitude'})
points_df = points_df.rename(columns=lambda col: col.replace('_location_lat', '_lat').replace('_location_lon', '_lon'))
points_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>real_longitude</th>
      <th>real_latitude</th>
      <th>apple_lat</th>
      <th>apple_lon</th>
      <th>straw_lat</th>
      <th>straw_lon</th>
      <th>grapes_lat</th>
      <th>grapes_lon</th>
      <th>orange_lat</th>
      <th>orange_lon</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-80.140036</td>
      <td>25.970938</td>
      <td>25.967175</td>
      <td>-80.141671</td>
      <td>25.969655</td>
      <td>-80.150446</td>
      <td>25.935669</td>
      <td>-80.097432</td>
      <td>26.016421</td>
      <td>-80.081083</td>
    </tr>
    <tr>
      <th>1</th>
      <td>-74.078844</td>
      <td>40.843007</td>
      <td>40.837265</td>
      <td>-74.069981</td>
      <td>40.844138</td>
      <td>-74.070920</td>
      <td>40.861805</td>
      <td>-74.076038</td>
      <td>40.783237</td>
      <td>-74.019430</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-111.888251</td>
      <td>33.269223</td>
      <td>33.271840</td>
      <td>-111.884106</td>
      <td>33.261590</td>
      <td>-111.878941</td>
      <td>33.250445</td>
      <td>-111.931532</td>
      <td>33.237628</td>
      <td>-111.867397</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-93.269275</td>
      <td>44.966872</td>
      <td>44.968429</td>
      <td>-93.267415</td>
      <td>44.977544</td>
      <td>-93.268844</td>
      <td>44.934335</td>
      <td>-93.278985</td>
      <td>44.970295</td>
      <td>-93.255129</td>
    </tr>
    <tr>
      <th>4</th>
      <td>-75.391091</td>
      <td>40.100081</td>
      <td>40.086834</td>
      <td>-75.405068</td>
      <td>40.085259</td>
      <td>-75.410947</td>
      <td>40.059155</td>
      <td>-75.384579</td>
      <td>40.103281</td>
      <td>-75.339505</td>
    </tr>
  </tbody>
</table>
</div>




```python
from geopy.geocoders import Nominatim # Nominatim is OpenStreetMap's nomeclator

def get_country(geolocator, lat, lon):
    """
    Get the country for a given `latitude` and `longitude`, using OSM Nominatim.

    Args:
        geolocator (geopy.geocoders): A geocoder object that is used to query OSM
        lat (str or float): The latitude of the Point of Interest
        lon (str or float): The longitude of the Point of Interest

    Returns:
        country (str): The country if found; None if error appeared, or

    """
    location = geolocator.reverse((lat, lon), language='en', timeout=10)
    if location and 'country' in location.raw['address']:
        country = location.raw['address']['country']
    else: country = None

    return country

# Define the geolocator, which we will use to query OSM
# (OSM requires us to include a custom user agent to filter the requests)
geolocator = Nominatim(user_agent="axa-geocoder-exercise")

# Apply the country locator to each row
country_df = points_df.copy()
country_df['country'] = country_df.apply(lambda row: get_country(geolocator, row['real_latitude'], row['real_longitude']), axis=1)

country_df['country'].unique().tolist()
```




    ['United States']



As we can see, all of the samples are in the US! We can verify this by plotting the samples in a map:


```python
import folium         # Use leaflet.js (<3) to plot points on a map

def plot_points_on_map(df_geo, lat_col='latitude', lon_col='longitude', zoom=10,  center=None):
    """
    Plots df columns on a map using Leaflet.

    Args:
        df_geo (pd.DataFrame): The pandas dataframe with geolocation data, one point per row.
        lat_col (str): The column where we will locate the latitude of each sample
        center (tuple or list): An optional point to center the map in
        lon_col (str): The df column where we will locate the longitudes

    Returns:
        (folium.folium.Map): A leaflet map, with all the points plotted
    """
    if center is None: center = (df_geo[lat_col].mean(), df_geo[lon_col].mean())

    # Create map and add points to it
    m = folium.Map(location=center, zoom_start=zoom)
    for _, row in df_geo.iterrows():
        folium.Marker(
            location=[row[lat_col], row[lon_col]],
            popup=f"Lat: {row[lat_col]}, Lon: {row[lon_col]}"
        ).add_to(m)

    return m

plot_points_on_map(points_df, lat_col='real_latitude', lon_col='real_longitude', zoom = 3)
```

![Map of all of the samples, displayed as blue markers on a US white US Map over blue background](map_all_samples.png)

As a bonus, I will also search for the location of the samples using a local database, in a way akin to what I would be doing on my current job. The local DB has been downloaded from GADM, at [this link](https://gadm.org/download_world.html), and uses GeoPackage, an standard for spatial databases.

Be sure to download the "layers" package, which contains aggregated geometries per admin level. I normally use pre-processed versions of the GADM files, so, I'm not going to lie, it took me a bit to remember that!


```python
import geopandas as gpd # Like pandas, but with geo!

# Convert the existing points df to a geo dataframe
points_gdf = gpd.GeoDataFrame(points_df, crs="EPSG:4326",
    geometry = gpd.points_from_xy(points_df['real_longitude'], points_df['real_latitude'])
)

# Read the layer 0 (country) of the GeoPackage File:
countries_gdf = gpd.read_file("gadm_410-levels.gpkg", layer = "ADM_0")

# Perform a spatial join, and return the countries for the samples:
country_df = gpd.sjoin(points_gdf, countries_gdf, how="left", predicate="within")
print(country_df['COUNTRY'].unique().tolist())
```

    ['United States', nan]


It seems like there is a sample with nan! Lets see where it is:


```python
import numpy as np
weird_sample = country_df[country_df['COUNTRY'].isna()]
plot_points_on_map(weird_sample, lat_col='real_latitude', lon_col='real_longitude', zoom = 15)
```

![Map of a sample, overwater, next to the port of Seattle](isolated_sample.png)


The sample at fault seems to be *over the water*, which, probably, is out of the spatial geometries defined by the GADM file; but, since OpenStreetMap takes into account maritime borders, it didn't pose a problem when we used the online query.

While this is interesting - we probably identified a wrongly defined point -, it is out of the question that this sample is also under US Jurisdiction; so, we can conclude that the answer to the first question is: **all samples are located in the United States**

## Deciding which geocoder is the best

The simplest way of deciding which geocoder is the best, without being able to look at their source code, is by comparing **the distance between each geocoded sample and their real location**; the smaller this distance is, the better. We will plot these distances and see if there is any clear winner; if not, we would go on to discuss about which we can choose.

We have to take into account that the earth is a sphere when we calculate these distances; we cannot simply substract the values, since a degree of longitude does not equate the same number of meters all across the earth (because they converge at the poles), even if latitude-to-meter ratio is almost constant (be it not for the obloid shape of the earth).

Thus, the best way to proceed is to use a built-in package, such as **geopy** :


```python
from geopy.distance import geodesic

def calculate_distances_geopy(geo_df, geocoder):
    """
    Calculate the distance between the points in two dataframes.

    Args:
        geo_df(pd.DataFrame): A pandas table with 'real_latitude', 'real_latitude',
            as well as `geocoder`_lat and `geocoder`lon columns
        geocoder (str): The geocoder that we are evaluating

    Returns:
        results (pd.Series): The result of applying geopy.distance.geodesic to the coordinates,
            making a list of the distance (in kilometers) between the samples
    """
    distance = geo_df.apply(
        lambda row: geodesic(
            (row['real_latitude'], row['real_longitude']),
            (row[f'{geocoder}_lat'], row[f'{geocoder}_lon'])
        ).kilometers, axis=1
    )
    return distance

distance_df = pd.DataFrame()
for each_geocoder in ['apple', 'straw', 'orange', 'grapes']:
    distance_df[f'{each_geocoder}_distance'] = calculate_distances_geopy(points_df, each_geocoder)
distance_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>apple_distance</th>
      <th>straw_distance</th>
      <th>orange_distance</th>
      <th>grapes_distance</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.447936</td>
      <td>1.052193</td>
      <td>7.760885</td>
      <td>5.785715</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.982499</td>
      <td>0.679942</td>
      <td>8.317751</td>
      <td>2.100911</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.483059</td>
      <td>1.212093</td>
      <td>4.006921</td>
      <td>4.538910</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.226898</td>
      <td>1.186547</td>
      <td>1.179073</td>
      <td>3.696114</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1.893210</td>
      <td>2.361334</td>
      <td>4.412902</td>
      <td>4.578160</td>
    </tr>
  </tbody>
</table>
</div>



Once we have calculated the distances, we will plot a **whisker plot** with them:


```python
import matplotlib.pyplot as plt
import seaborn as sns

def plot_whiskers(df, columns):
    """
    Draw whisker plots from a df, using selected columns.

    Args:
        data_df (pandas.DataFrame): A DataFrame with the columns to draw.
        columns (list): A list of the columns to include in the

    Returns:
        none: It plots the desired figure
    """
    plt.figure(figsize=(12, 6))

    # Melt the df for Seaborn (creating a long-format table with columns Geocoder, Distance)
    melted_df = df[columns].melt(var_name='Geocoder', value_name='Distance')
    sns.boxplot(x='Geocoder', y='Distance', data=melted_df) # And create the plot

    # Add labels and plot; plt.tight_layout() is optional, to optimize space
    plt.title("Geocoder Distances' Whisker Plot", fontsize=16)
    plt.xlabel('Geocoder', fontsize=14)
    plt.ylabel('Distance', fontsize=14)
    plt.tight_layout(); plt.show()

plot_whiskers(distance_df, list(distance_df.columns))
```

![Whisker plots for "apple, orange, grapes and straw" geocoders. For a detailed analysis, see below.](whisker_plots.png)


We can already see that two geocoders, **grapes and orange**, are definetely not the best, in terms of minimizing the distance differential with the real data. However, **apple and straw** seem to be much closer, with apple having an slightly smaller distance average to the real data than straw.

We can confirm this by looking at pandas' `df.describe()`, which will give us the numeric values for each geocoder:


```python
distance_df.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>apple_distance</th>
      <th>straw_distance</th>
      <th>orange_distance</th>
      <th>grapes_distance</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>100.000000</td>
      <td>100.000000</td>
      <td>100.000000</td>
      <td>100.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>1.040814</td>
      <td>1.246609</td>
      <td>7.575019</td>
      <td>3.632016</td>
    </tr>
    <tr>
      <th>std</th>
      <td>1.195052</td>
      <td>1.034627</td>
      <td>3.116735</td>
      <td>2.379182</td>
    </tr>
    <tr>
      <th>min</th>
      <td>0.165324</td>
      <td>0.219300</td>
      <td>0.769312</td>
      <td>0.155740</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>0.620179</td>
      <td>0.768911</td>
      <td>5.427451</td>
      <td>2.002649</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>0.896274</td>
      <td>1.122884</td>
      <td>7.960971</td>
      <td>3.494060</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>1.188870</td>
      <td>1.382767</td>
      <td>9.703418</td>
      <td>4.911116</td>
    </tr>
    <tr>
      <th>max</th>
      <td>12.061384</td>
      <td>9.150569</td>
      <td>16.696743</td>
      <td>20.008344</td>
    </tr>
  </tbody>
</table>
</div>


We see that **apple is definetely slightly better than straw**, with a mean that could be even better were it not for a big outlier of 12 km. This is, I would argue, even a positive point; if the geocoder has *only a few points with a huge error*, it would be easier to detect them (at least, visually) than if it had *more points with an smaller error* (as is the case with straw), even if the means remain pretty simmilar.

The **distribution** of the values in both geocoders seems to be pretty close to the average, even a bit more centered in the case of apple, which has lower distance limits for each quartile.

Taking all of this into account, I would recommend using **apple** as geocoder for our data; ranking them according to the mean of the distances to ground truth, we would get: **apple > straw >> grapes >>> orange**
