---
title: "Cuando la alondra se fue"
date: 2024-02-14
menu:
  sidebar:
    name:   Fábula
    identifier: fabula
    parent: poemas
    weight: 40
author:
  name: Mr Framework
  image: /images/author/Mr Framework.png
---

Despertaba, sudoroso, de un confuso sueño, de un viaje de una noche de verano que casi parecía realidad.

El doctor se había convertido en un cuco, que, afanoso, intentaba ocultar en el nido de un jilguero la llave de su amor.

Ramita a ramita se afanaba el cuco, escondiendo entre las pajas los secretos a incubar: ni tan profundo como para que se hielen, ni tan arriba como para que se vean, en el lugar exacto donde pudieran ser empollados hasta la próxima primavera.

Posóse entonces en el árbol una alondra, que, juguetona, comía la fruta colgada de sus ramas:

- Quid hic faciet -se preguntaba el cuco- una pájara tan hermosa? Tal vez ella quiera cuidarme la llave.

Y, saltando aprisa, fue a hablar con la alondra. Hablaron y hablaron, y pronto se pasó el invierno; la pasión, que quemaba sus corazones, parecía haberlo derretido. Llegó entonces, inexorable, la primavera, llena de flores y brisas cálidas; y la alondra, hambrienta, se fue a buscar cereal. Buscó y buscó, atravesando de lado a lado los anchos campos de Castilla; y, cuando al fin encontró su tesoro en un granero, no supo volver con el cuco, quien, absorto en sus amores, se había olvidado de emigrar.

Esperó y esperó, días y después semanas, pero la alondra no regresaba; y, sólo como estaba, perdido en tierras inhóspitas, recordó al jilguero.

Y fue, sólo entonces, cuando el cuco se dio cuenta:

Cuando la alondra se fue, el jilguero ya no estaba allí.

---

📸 :  [Vidar Nordli-Mathisen](https://unsplash.com/es/@vidarnm) on [Unsplash](https://unsplash.com/es/fotos/pajaro-amarillo-y-negro-en-comedero-gris-para-pajaros-L4iALN9-OhE)

#poesia #poetry #fabula #fable #poesiaenespañol

{{<mastobutton>}}
