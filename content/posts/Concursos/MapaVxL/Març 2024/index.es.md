---
title: "Mapa d'Establiments VxL"
date: 2024-03-05
menu:
  sidebar:
    name: Març 2024
    identifier: mapavxl_març24
    parent: mapavxl
    weight: 40
---

{{< badge href="https://codeberg.org/FlyingFlamingo/mapa-vxl" badge="codeberg" >}}

</br>

El Voluntariat per la Lengua és un programa de la Generalitat de Catalunya, que, a través del Centre per a la Normalització Lingüística, fomenta i la pràctica del català en el dia al dia.

Per ello, proposa als interessats el visitar una sèrie de "Entitats col·laboradors", on els dependients coneixen el programa i estan disposats a parlar en català.

L'assistència a tres d'aquests comerços és obligatòria per aprovar el Curs de Bàsic 3 (A2) de Catalán al CPNL, però el layout de la web, que no disposa d'un mapa, dificulta l'ús del recurs, ja que resulta complicat trobar-lo. establiments per zona.

Per això, he decidit analitzar aquestes dades, generant un KML que pot importar després en un servei o app de mapes, per servir com a guia.

En el meu cas, [lo he importat en FramaCarte](https://framacarte.org/es/map/establiments-collaboradors-vxl_181936#15/41.3847/2.1709).

Com que em mola molt el codi obert, he pujat l'script per actualitzar el mapa a codeberg; pots trobar-lo clicant més amunt. El mapa és aquí:

<iframe width="100%" height="500px" frameborder="0" allowfullscreen allow="geolocation" src="https://framacarte.org/es/map/establiments-collaboradors-vxl_181936?scaleControl=false&miniMap=true&scrollWheelZoom=false&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=caption&captionBar=false&captionMenus=false#15/41.3847/2.1709"></iframe><p><a href="//framacarte.org/es/map/establiments-collaboradors-vxl_181936?scaleControl=false&miniMap=true&scrollWheelZoom=true&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=caption&captionBar=false&captionMenus=false#15/41.3847/2.1709">Ver pantalla completa</a></p>
