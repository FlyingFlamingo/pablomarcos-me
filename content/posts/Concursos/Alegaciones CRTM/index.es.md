---
title: "Alegaciones al Mapa Concesional del CRTM en 2024"
date: 2024-08-28
menu:
  sidebar:
    name: Alegaciones CRTM
    identifier: alegacionesCRTM2024
    parent: concursos
    weight: 40
---

Con motivo de la re-licitación de las Concesiones de Autobús del Consorcio Regional de Transportes de Madrid, que acababan en 2024, he redactado una serie de alegaciones, con la intención de mejorar el servicio de manera práctica a través de pequeños cambios, visto desde el punto de vista de un usuario habitual.

El resultado es la presentación que se ve más abajo, y que está aún en proceso de revisión por pares; puedes clicar ? tras clicar en ella para ver cómo navegar por la misma:

<div class="fluidMedia">
    <iframe src="/es/presentations/alegaciones-crtm" frameborder="0"> </iframe>
</div>


<style>

.fluidMedia {
    position: relative;
    padding-bottom: 56.25%; /* proportion value to aspect ratio 16:9 (9 / 16 = 0.5625 or 56.25%) */
    height: 0;
    overflow: hidden;
}

.fluidMedia iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

</style>

