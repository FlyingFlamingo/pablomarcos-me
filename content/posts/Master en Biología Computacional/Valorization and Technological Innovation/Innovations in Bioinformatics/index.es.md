---
title: "Innovations in Bioinformatics"
author: Pablo Marcos
date: 2022-03-22
math: true
menu:
  sidebar:
    name: Bioinfo Innovations
    identifier: bioinfo_innovations
    parent: valorization_innovation
    weight: 40
---


The first task in the Valorization and Technological Innovation subject was to present a case of an innovative company to the class. Since I have always been a bit of a rebel, I presented [theranos](https://es.wikipedia.org/wiki/Theranos):

{{< embed-pdf src="/es/posts/master-en-biología-computacional/valorization-and-technological-innovation/innovations-in-bioinformatics/Innovation.pdf" >}}
