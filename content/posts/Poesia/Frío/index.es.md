---
title: "Frío"
date: 2024-04-02
menu:
  sidebar:
    name:   Frío
    identifier: frio
    parent: poemas
    weight: 40
author:
  name: Mr Framework
  image: /images/author/Mr Framework.png
---

El lunes amanecí helado, pues te se quedó abierta la ventana por la que huyeron de mi amor

El martes, cuando intenté cerrarla, me corté la mano por error

El miércoles me cambié de casa, crucé el Himalaya y vencí al dragón

El jueves encendí una vela especiada, cuya esperanza me ilumina desde entonces, verdadero foco eterno que ilumina el renovado escenario de un cuerpo recompuesto

El viernes descansé, oliendo en el verde de la hierba las notas pausadas de un mundo que se mueve.

El sábado renové los votos que hice en Torino, hasta que la muerte nos separe

Y ahora que es ya domingo, cuando estaba apunto de irme de nuevo a la cama, acabo de ver que la ventana sigue abierta, y he pensado, he querido recordar, a los dragones que maté, a las nubes de salitre que crucé en el Shangri-La, a las siete pruebas en las que me he jugado el tipo; y he pensado, he querido recordar, que el frío es tan sólo la ausencia de calor.

Que la radio sigue sonando, que las hogueras no se han apagado aún, sino que brillan cada vez con más fuerza, devorando los muebles viejos de 100 años de soledad.

Y he sentido, al cerrar los ojos, que estaba otra vez vivo, y que, si sigo a tu lado, amigo, no habrá ni sombra del frío que una noche se coló por mi ventana.

---

📸 :  [Tadeusz Lakota](https://unsplash.com/es/@tadekl) on [Unsplash](https://unsplash.com/es/fotos/perro-adulto-de-pelo-corto-sentado-en-la-nieve-mientras-usa-sombrero-naranja-y-negro-eV_iPlkHwgQ); [Unsplash License](https://unsplash.com/es/licencia)

#poesia #poetry #frio #cold #poesiaenespañol

{{<mastobutton>}}
