---
title: "Innovation in my TFM"
author: Pablo Marcos
date: 2022-04-26
math: true
menu:
  sidebar:
    name: Innovation in my TFM
    identifier: innovation_tfm
    parent: valorization_innovation
    weight: 40
---


As part of the Valorization and Technological Innovation subject, we were told to present our Final Master Thesis as if it were a company's pitch, in order to learn how to do it. Here is the presentation I gave:

{{< embed-pdf src="/es/posts/master-en-biología-computacional/valorization-and-technological-innovation/my-innovative-tfm/My Innovative TFM.pdf" >}}
