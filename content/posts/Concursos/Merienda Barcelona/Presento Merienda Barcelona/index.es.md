---
title: "Presento Merienda Barcelona"
date: 2024-02-25
menu:
  sidebar:
    name: Presento Merienda Barcelona
    identifier: presentomeriendabcn
    parent: meriendabcn
author:
  name: Merienda Barcelona
  image: /images/author/Logo Merienda Barcelona.png
---

Hoy, he decidido dar forma a un proyecto que llevaba tiempo barajando, que es el de volver a hacer voluntariado de reparto de alimentos a personas sin hogar. Es algo que he hecho muchos años, primero (2017-2019) en [Reparto Solidario Universitario](https://www.instagram.com/repartosolidariouniversitario/), y después en [Madrid Reparte](https://www.instagram.com/madrid_reparte/) (su evolución natural, que cerró hace unos meses); y, en 2021, en [Bokatas](https://bokatas.org/).

Sin embargo, como estos últimos años he estado en Lyon y Barcelona, y como no conozco asociaciones que hagan esto mismo por aquí, he estado unos años sin hacer Voluntariado, y es algo que, ahora que tengo más estabilidad en Barcelona, me gustaría retomar. Para ello, he decidido empezar poco a poco, repartiendo primero un poco de caldo o café caliente a los usuarios; y, si les gusta, puede que me plantee hornear un bizcocho o hacer algún plato más complejo de cena.

Las ideas claves de este voluntariado serán:

* **Compromiso**: Una hora a la semana en la que repartir alimentos a los usuarios, en un horario que sea fiable y constante, avisando a los usuarios de potenciales cambios con al menos una semana de antelación. De aquí nace la idea de escribir esto en el blog, pues lo veo como una manera de forzarme a tomar la responsabilidad de ir a una hora fija cada semana.
* **Flexibilidad**: Quiero que sea una cosa que me haga ilusión, y que, aún manteniendo el compromiso, no se me haga pesado; por ello (avisando con antelación), haré varios tests para ver si los usuarios prefieren caldo o café, por las mañanas o por las tardes, y acompañado de un bizcocho, bocadillo, o algo de cena.
* **Cariño**: A veces, hablar es casi tan importante como comer, y quiero que esta actividad se base no sólo en llevar algún alimento caliente para combatir el frío, sino, también, si los usuarios lo solicitan, en llevar algo de conversación para combatir el frío metafórico de la soledad.

Empezando la semana del 04.03 (cuando ya tenga mi termo), iré actualizando este post, indicando qué he hecho cada día y qué tal ha ido la ronda.

También subiré posts semanales [a una cuenta separada que he creado en Mastodon](https://masto.es/@MeriendaBarcelona).

## Semana del 04.03

Esta primera semana, he empezado con calma: he comprado unos bricks de caldo de pollo, y, tras calentarlo, lo he repartido cerca de mi casa, en Sants Estació. He podido atender a 2 usuarios, de los cuales 3 han tomado caldo; la semana que viene, probaré a llevar café caliente y galletas, a ver si tiene más aceptación. Sin embargo, creo que tendrá que ser el miércoles, ya que el lunes estaré en Madrid :p

{{< img src="/es/posts/concursos/merienda-barcelona/presento-merienda-barcelona/04.03.jpeg" width="70%" align="center" title="Termo con caldo de pollo, listo par la aventura!" >}}

## Semana del 11.03

Esta segunda semana, he llevado café calentito con galletas, a ver si tenía mejor aceptación entre los usuarios que el caldo. Ha sido un e-xi-ta-zo! De 5 usuarios atendidos, los 5 han cogido algo; 4 café y galletas, y 1 que sólamente quiso galletas.

Un usuario que vive del lado de la estación me ha pedido dinero para comer; y, aunque la idea es no realizar aportaciones económicas, me he planteado llevar sándwiches mixtos para la próxima semana. También veo que podría salir bien llevar fruta (manzanas, por ejemplo) picadas y envueltas, que además es súper sanito.

De momento, eso es todo. Hasta la próxima semana!

{{< img src="/es/posts/concursos/merienda-barcelona/presento-merienda-barcelona/12.03.jpeg" width="70%" align="center" title="Café con el favorito, lotus biscoff" >}}

## Semana del 18.03

De nuevo, ¡un éxito! He atendido a 6 usuarios este lunes, y todos han tomado café y unos Sandwiches Mixtos que hice [con la receta de Helio Roque](https://www.tiktok.com/@helioroque_/video/7333727736357326113). Me quedaron super ricos!

Además, he disfrutado mucho de hablar con los usuarios; uno de ellos incluso me ha enseñado fotos de sus hijas, y otra, muy amable, me ha hablado en Francés - hace tanto que no lo practicaba! - para comentarme que aquí, en Barcelona, brilla mucho más el sol que en París. ¡Que cuqui!

El objetivo para la semana que viene: un menú mas sano, con manzana cortada y sandwich vegetal 💃

{{< img src="/es/posts/concursos/merienda-barcelona/presento-merienda-barcelona/18.03.jpeg" width="70%" align="center" title="Sandwich mixto, café con leche de avena y bollito; el menú de la semana." >}}

## Semana del 25.03

Esta semana, he preparado un sandwich vegetal con tomate, bonito, huevo, lechuga, mayonesa y pan tostado, con la intención de ofrecer a los usuarios una opción algo más saludable; y, claro, tampoco ha faltado el clásico café, acompañado esta vez de una 🍃 saludable manzana picadita.

Sin embargo, esta tarde ha sido lluviosa en Barcelona (por fin!) por lo que no he encontrado a muchos de los usuarios habituales; por suerte, he podido tender a otras 6 personas, practicar un poco de francés, y llevarles una bebida calentita!

La semana que viene, empieza un nuevo mes, y quiero llevar algún dulce especial de Semana Santa; veremos qué tal se me da!

Con respecto a la web, voy a crear un post nuevo por mes, para que no se alargue este en exceso, y para mostrar poco a poco el progreso :p

{{< img src="/es/posts/concursos/merienda-barcelona/presento-merienda-barcelona/25.03.jpeg" width="70%" align="center" title="Sandwich vegetal, con manzana picada y café." >}}

---

📸 de Cabecera: [Aaron Doucet](https://unsplash.com/es/@adoucett) en [Unsplash](https://unsplash.com/es/fotos/mujer-con-camisa-verde-de-manga-larga-de-pie-frente-a-frascos-de-vidrio-transparente-HkbeLxOJlqk), Unsplash License
