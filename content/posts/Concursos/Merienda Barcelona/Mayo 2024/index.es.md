---
title: "Merienda Barcelona - Mayo 2024"
date: 2024-05-07
menu:
  sidebar:
    name: Mayo 2024
    identifier: mb_mayo2024
    parent: meriendabcn
author:
  name: Merienda Barcelona
  image: /images/author/Logo Merienda Barcelona.png
---

Continúo con el diario de mis experiencias con el Proyecto Merienda Barcelona:

## Semana del 06.05

Esta semana, por motivos operativos, tuve que mover la fecha de reparto al viernes 10.05; como receta, repetí el [Bocadillo de Burrata e Higos de Gypsy Chef](https://www.youtube.com/watch?v=5NeIyz-8NxA), tuneado con pan de sandwich, queso de cabra y melón en lugar de higos. De acompañamiento, llevé un _cruasan_, fresas y café de leche de coco; ¡rico!

{{< img src="/es/posts/concursos/merienda-barcelona/mayo-2024/10.05.jpeg" width="70%" align="center" title="Sandwich de melón y queso de cabra, con café de coco, fresas y quackson." >}}

## Semana del 13.05

Esta semana, preparé un sabroso sandwich de pollo con lechuga y alioli, que, pese a su sencillez, estaba potentorro de sabor. Ayudé a unos 8 usuarios, y conocí a un grupo nuevo de europeos (suecos, polacos, rumanos e indios incluidos) con los que me lo pasé genial aprendiendo trucos de magia. Fue brutal!

{{< img src="/es/posts/concursos/merienda-barcelona/mayo-2024/13.05.jpeg" width="70%" align="center" title="Bocadillo de Pollo con Ali-Oli y Lechuga, y Zumo de Naranja de bebida." >}}

## Semana del 20.05

Hoy, ha sido un día lleno de sorpesas! He conocido a un nuevo usuario uruguayo, he sido identificado por los Mosos (que buscaban info de un robo que acababa de pasar) y he logrado atender a 9 usuarios! Uno de los cuales,  por cierto, no había comido nada en todo el día, remarcando la importancia de este servicio a la gente de Sants.

En cuanto al menú, hice, _de motu propio_, un Trikini a la plancha de pavo, mozarella, ruculla y trufa, tostado en mantequilla. Quedó riquísimo! Debido al calor, lo acompañé de zumo de naranja, aunque parece que los usuarios hubieran preferido café (nota para el futuro).

{{< img src="/es/posts/concursos/merienda-barcelona/mayo-2024/20.05.jpeg" width="70%" align="center" title="Trikini a la plancha de pavo, mozarella, ruculla y trufa, con zumo de naranja." >}}


## Semana del 27.05

Récord histórico de usuarios en el servicio! Hoy, he logrado ayudar a... ¡12 personas!

Y, ¿cómo ha sido posible? Pues, la semana pasada, se me ocurrió hacer una _colabo_ más falsa que las de Paquita Salas: los alimentos que he repartido venían, en su mayor parte, de Too Good to Go!

Esta app, de la que soy ADICTO, ofrece packs de comida descontada; y, como hay _muchíiiiisimas_ panaderías en Barcelona con este servicio, y como yo al final lo que llevo no dejan de ser productos de panadería, decidí coger 3 packs de una cadena de establecimientos barceloneses, el 365, y ver qué tal se daba.

Como comento, en cuanto a cantidad de alimentos es exitazo, ya que he podido contar con _mucha_ bollería; además, los bocadillos, que me preocupaban en caso de que alguien tuviese los dientes mal, en realidad han tenido muy buena aceptación.

A nivel gastos, sin embargo, no ha supesto mucho ahorro: como me daba miedo que los usuarios no quisiesen pan pan, hice sandwiches de tomate y atún, y, además, puse de acompañamiento fresas (el toque sano) y café con leche de avena (el toque líquido). Por otro lado, me ha gustado el ahorro que supone para el medio ambiente (al reducir los plásticos de un solo uso y el desperdicio alimentario). Es posible que repita próximamente.

De cara a la semana que viene, he tenido que avisar a los usuarios de que no podré ir, ya que el lunes estaré en Madrid, visitando a la familia; y, el viernes (la única otra tarde libre) tengo cita para donar sangre.

{{< img src="/es/posts/concursos/merienda-barcelona/mayo-2024/27.05.jpeg" width="70%" align="center" title="Bollería del 365 @ Too Good to Go, café de leche de avena y sandwich integral de atún y tomate." >}}


---

📸 de Cabecera: [Logan Armstrong](https://unsplash.com/es/@loganstrongarms) en [Unsplash](https://unsplash.com/es/fotos/vista-aerea-de-los-edificios-de-la-ciudad-durante-el-dia-hVhfqhDYciU), [Unsplash License](https://unsplash.com/es/licencia)

