---
title: "PCBakers - 2024"
date: 2024-04-19
menu:
  sidebar:
    name: 2024
    identifier: pcbakers_2024
    parent: pcbakers
---


---

Continúo contando mis aventuras en la Bake Sale, este 2024 que tengo mucho más tiempo y relax tras pasar el insoportable _impasse_ que supuso el IRB.

## 24.01 - DTI Foundation

El año empezó un poco desastroso: aunque decidí hacer la [Whole Orange Snack Cake del New York Times](https://cooking.nytimes.com/recipes/1022002-whole-orange-snack-cake), se me rompió el tupper de cristal en la que lo llevaba justo en la puerta del PCB, con lo que decidí no entregarla (no sea que alguien se haga daño)

{{< img src="/es/posts/concursos/pcbakers/2024/24.01.jpg" width="70%" align="center" title="Un plato roto, mi aportación al Bake Sale de enero." >}}

## 19.02 - GAEM

En Febrero, quise repetir la Orange Cake de enero, que, esta vez sí, pude entregar sin problemas. Sin embargo, parece que me olvidé de tomar foto, y la única imagen que tengo de la misma es esta que dejo abajo, de su versión mini acompañando una cena que me hice (la que llevé era como el doble de grande)

{{< img src="/es/posts/concursos/pcbakers/2024/19.02.jpeg" width="70%" align="center" title="Plato de arroz con verduras y, a la izquierda arriba, un mini-bizcochito de naranja con frosting naranja." >}}

## 21.03 - Pulseras Candelas

Para marzo, probé la [Tarta de Lima y Pistacho de El Comidista](https://www.youtube.com/watch?v=v5sBe9ipr9M), que, la verdad, para el poco esfuerzo que cuesta es SUPER resultona (aunqe yo personalmente prefiero añadirle gelatina para extra de dureza). En cuanto la pusimos a la venta, voló!

{{< img src="/es/posts/concursos/pcbakers/2024/21.03.jpeg" width="70%" align="center" title="Tarta de queso blanca con la mirad cubierta de pistacho, dos rodajas de lima, y un cartel que indica sus componentes." >}}

## 18.04 - Proyecto Afrovaca

Esta semana me pilló algo liado, por lo que opté por repetir la tarta de lima y pistacho de la semana pasada. De nuevo, no duró ni 2 minutos a la venta; ¡exitazo!

{{< img src="/es/posts/concursos/pcbakers/2024/18.04.jpeg" width="70%" align="center" title="Tarta de queso de lima y pistacho parcialmente cortada y vendida." >}}

## 22.05 -  Mediterranea Saving Humans

La charity de este mes me parece especialmente guay, ya que soy bastante fan de Open Arms, su equivalente español. Para probar cosas nuevas, hice la [Tarta de Santiago de El Comidista](https://elpais.com/gastronomia/recetas/2019/07/02/receta/1562086508_979264.html#), que no sólo es facilísima, sino que, además, es barata y cunde _mogollón_. ¡Me encantó cómo queda en la air fryer!

{{< img src="/es/posts/concursos/pcbakers/2024/22.05.jpeg" width="70%" align="center" title="Tres pequeñas Tartas de Santiago" >}}

## 13.06 - Vocation for the Homeland

Para estes mes, los organizadores de PCBakers eligieron [Vocation for the Homeland](https://www.homelend.com.ua/), una organización dedicada, según ellos mismos, al "apoyo humanitario al personal de guerra luchando en ucrania". Yo soy pacifista, y considero que apoyar al ejército es bastante inmoral, por lo que he decidido donar el coste que me genera hacer la tarta (12.50 $) a la [Cruz Roja Ucraniana](https://redcross.org.ua/en/donate/), y ya veré si hago una tarta para regalar a alguien o no; si me acuerdo, postearé foto :p

## 03.07 - Fundación Índigo

Para ayudar a la [Fundación Índigo](https://asociacionindigo.com/), este mes intenté hacer [esta simpática Cheesecake japonesa](https://www.youtube.com/watch?v=r4-n8tkJGfc). Como, tras dos horas, no subía NADA, la convertí de EMERGENCIA 🚨 en una cheescake al uso; que, aunque con PINTÓN, debo revelar que no quedó muy sabrosa (creo que le faltó pizca de sal y un queso más intenso).

Con esta Bake Sale, se interrumpe el proyecto hasta Septiembre. Nos vemos entonces (o tal vez Octubre 📆) con más, pero no mejor, porque es imposible!

{{< img src="/es/posts/concursos/pcbakers/2024/03.07.jpeg" width="70%" align="center" title="Tarta de queso, amarilla y tostadita" >}}

## 29.10 - Gats de Grácia

Tras el parón del verano, ¡vuelve PCBakers! Esta vez, como en Octubre del año pasado, ayudamos a [Gats de Grácia](https://www.gatsdegracia.cat/), un refugio gatuno muy cuqui! Además, como ví que el motivo de la Bake Sale era "animalista", contacté a unos compañeros de la [ILP noesmicultura.org Contra la Tauromaquia](https://noesmicultura.org/es) para recoger firmas simultáneamente.

¡Conseguimos más de 500 euros y 70 firmas! 💪 Con suerte esto lleve a mejoras en la vida de muchos animalitos 🐮😼

Respecto a la tarta, cociné dos [Pastieras Napoletanas de El Comidista](https://www.youtube.com/watch?v=OxMG92Ei3QQ), que (gracias a los 200grs de azúcar que llevaba cada una), estaban bastante buenas! Como en españa no venden grano cotto, lo cambié por Quinoa con Leche jejeje.

{{< img src="/es/posts/concursos/pcbakers/2024/29.10.jpeg" width="70%" align="center" title="Pastiera Napoletana de la Mamma" >}}

## 12.11 - Emergency Sale for Valencia

Ante la [catástrofe de la DANA](https://www.eldiario.es/sociedad/ultima-hora-dana-directo_6_11780269.html), PCBakers no se queda de brazos cruzados! Organiamos una Bake Sale de emergencia, cuyos beneficios irán destinados a [SPAB Burjassot](https://protectoraburjassot.com/) y [Acción Contra el Hambre](https://accioncontraelhambre.org/es). Hemos recaudado más de 600 euros!

Yo hice una [Tarta de San Martiño](https://www.lavanguardia.com/comer/recetas/20201203/6079212/como-tarta-castanas-san-martino.html) con [Nutella de Castañas](https://www.youtube.com/watch?v=JACTLcNwaO0), para celebrar las raíces gallegas de mi familia. Quedó buenísima! Aunque os recomiendo tener cuidado al hacerla, ya que me destrocé las uñas al pelar 💅

{{< img src="/es/posts/concursos/pcbakers/2024/12.11.jpeg" width="70%" align="center" title="Tartiña de San Martiño" >}}

## 20.11 - Malaika Solidaria

Este mes, repetimos causa con [Malaika Solidaria](https://www.castelldefels.org/es/servicios/solidaridad-y-cooperacion/consejo-de-paz-y-solidaridad/entidades-consejo/malaika-solidaria-asociacion), un colectivo cuyo objetivo es la promoción de la infancia, tanto desde el desarrollo educativo como desde el acceso a una nutrición adecuada para su crecimiento y a unas condiciones sanitarias adecuadas para su salud.

Para la ocasión, quise hacer un guiño a los orígenes de mi familia en [San Xoan de Rio](http://galicia.villasenflor.com/poblacion/san-xoan-de-rio/), con una típica [Bica Mantecada](https://www.youtube.com/watch?v=Jz4nApXtPfE), que me quedó ¡riquísima!

{{< img src="/es/posts/concursos/pcbakers/2024/20.11.jpeg" width="70%" align="center" title="Bica Mantecada de San Xoan de Rio, recubierta de azúcar glas, en una mesa de madera." >}}

---

📸 de Cabecera: [Siami Tam](https://unsplash.com/es/@siamialtrice_) en [Unsplash](https://unsplash.com/es/fotos/lote-de-tarta-de-bayas-cMhAZqjfE2w), Unsplash License
