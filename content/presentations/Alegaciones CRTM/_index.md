---
title: Alegaciones CRTM 2024
subtitle: Mejoras para el Transporte en Madrid Oeste
outputs: Reveal
reveal_hugo:
  custom_theme: /reveal-themes/transportation/styles.css
  margin: 0.2
  width: 1200
  height: 675
  slide_number: true
  highlight_theme: color-brewer
  transition: slide
  transition_speed: medium
  templates:
    hotpink:
      class: hotpink
      background: '#FF4081'

---

{{< slide background-image="/reveal-themes/transportation/header-bus.jpg" >}}

</br></br></br></br></br></br>

  # Alegaciones al mapa concesional

  ### Anteproyecto CM-61

  ~ [Pablo Ignacio Marcos López](https://www.pablomarcos.me/)



---

{{% section %}}
{{< slide background-image="/reveal-themes/transportation/bus-1.jpg" >}}

<div style="display: flex" class="row">
  <div style="flex: 45%" class="column"></div>
  <div style="flex: 55%" class="column">

  <h1 style="font-size:20rem;font-weight: bolder;color:#4AAF91"> 1 </h1>

  ### Objeto de las Alegaciones
  </div>
</div>

---

{{< slide background-image="/reveal-themes/transportation/text-1.jpg" >}}

* El objetivo de estas alegaciones es proponer mejoras que, desde el punto de vista de un usuario habitual, podrían mejorar enormemente la competitividad, y, por tanto, el aprovechamiento y el uso, del servicio de transporte interurbano del área de Pozuelo (Anteproyecto CM-61)

* Así, se proponen una serie de actuaciones de coste reducido, que, sin embargo, podrían mejorar el mallado de la red y la capacidad de captación de usuarios

---

{{< slide background-image="/reveal-themes/transportation/text-5.jpg" >}}

* También quisiera aprovechar para solicitar una cosa que, aunque ajena a estas alegaciones, me parece esencial que alguien en el Conserjería de Transportes sepa: el ML2 **NO** está cadenciado con el Cercanías en la Estación de Pozuelo, lo que dificulta enormemente su uso: es habitual llegar cuando este se está marchando, sufriendo la desagradable sensación de perderlo "en la cara"

* Y, por supuesto, dar las gracias a todos los que han contribuido en el diseño de estas alegaciones

{{% /section %}}

---

{{% section %}}
{{< slide background-image="/reveal-themes/transportation/bus-2.jpg" >}}

<div style="display: flex" class="row">
  <div style="flex: 55%" class="column">

  <h1 style="font-size:20rem;font-weight: bolder;color:#4AAF91"> 2 </h1>

  ### Línea 657-A
  </div>
  <div style="flex: 45%" class="column"></div>
</div>

---

{{< slide background-image="/reveal-themes/transportation/text-2.jpg" >}}

* 🔝 Esta línea resulta muy positiva por su elevada velocidad comercial y lo directa que es, permitiendo reducir enormemente el tiempo del trayecto Pozuelo -> Moncloa

* ✅ Sin embargo, se presenta una posibilidad de mejora, ampliando su recorrido con una parada más para dar servicio, no sólo a los universitarios del ESIC, sino también a los de la zona de Moncloa.

* 🚌 Incluyendo una parada en Cardenal Cisneros (ver mapa), se evita la necesidad de acudir a Moncloa a coger el bus, lo que puede suponer un ahorro de más de 10 minutos, mejorando mucho la competitividad de la línea

* 💰 El coste sería extremadamente reducido, ya que las paradas propuestas ya tienen marquesina, y el bus pasa por delante, aunque sin realizar parada.

---

{{< slide background-image="/reveal-themes/transportation/text-1.jpg">}}

<img src="657A Pozuelo - Moncloa.png">

---

{{< slide background-image="/reveal-themes/transportation/text-3.jpg">}}

<img src="657A Moncloa - Pozuelo.png">

{{% /section %}}

---

{{% section %}}
{{< slide background-image="/reveal-themes/transportation/bus-3.jpg" >}}

<div style="display: flex" class="row">
  <div style="flex: 45%" class="column"></div>
  <div style="flex: 55%" class="column">

  <h1 style="font-size:20rem;font-weight: bolder;color:#4AAF91"> 3 </h1>

  ### Nocturnos:
  ### N-902 & 656-A

  </div>

</div>

---

{{< slide background-image="/reveal-themes/transportation/text-6.jpg" auto-animate="" style="text-align: left;">}}

* ⏰ La problemática principal de estas líneas es su escasa frecuencia en fin de semana: al pasar cada 1h30m, los usuarios se ven forzados a decidir de antemano cuanto tiempo permanecer en Madrid, o a coger un VTC

* ⏫ Se propone aumentar su frecuencia al doble, es decir, un servicio cada 45m en fines de semana, posibilitando captar usuarios del vehículo privado o VTC

* 🗃 Otra posibilidad sería cadenciar ambas líneas con la N-901 / N-906, permitiendo a los usuarios llegar a Pozuelo sin tanta espera y continuar desde ahí el recorrido a pie; esto maximizaría la utilidad de las líneas para todo el área comprendida entre la Avda. de Europa y la Avda. de Juan XXIII

* 💰 El coste sería intermedio (más horas extra con nocturnidad), pero redundaría en beneficio de los usuarios, y en la utilidad de la red de transporte

{{% /section %}}

---

{{% section %}}
{{< slide background-image="/reveal-themes/transportation/bus-4.jpg" >}}

<div style="display: flex" class="row">
  <div style="flex: 55%" class="column">

  <h1 style="font-size:20rem;font-weight: bolder;color:#4AAF91"> 4 </h1>

  ### Línea L1
  ### (Urbano Pozuelo)

  </div>
  <div style="flex: 45%" class="column"></div>

</div>

---

{{< slide background-image="/reveal-themes/transportation/text-1.jpg" auto-animate="" style="text-align: left;">}}


* 🚄 En esta línea, se observa como punto muy positivo su extensión hasta la Estación de Cercanías, lo que permite aumentar enormemente la intermodalidad del servicio

* 🏫 Con la próxima apertura de la Universidad Camilo José Cela en Monteclaro, se plantea la posibilidad de utilizarla como lanzadera a dicha Estación, lo que permitiría mejorar la conexión de los vecinos de La Cabaña y el aprovechamiento de la línea con un escaso gasto adicional

* 🚶🏻 Se propone añadir una señalización peatonal desde la Universidad hasta la parada más cercana (Guipúzcoa-Toledo), así como extender el itinerario 11502 para llegar hasta dicha parada, facilitando su uso a los universitarios (con una caminata estimada de 5 minutos) y a los pacientes del hospital

* 💰 El coste sería extremadamente reducido, ya que las paradas propuestas ya tienen marquesina y/o indicador, y el recorrido extra del itinerario 11502 sería de sólo unos 5 minutos

---

{{< slide background-image="/reveal-themes/transportation/text-2.jpg">}}

<img src="L1 Cabaña - Estación.png">

---

{{< slide background-image="/reveal-themes/transportation/text-6.jpg">}}

<img src="L1 Estación - Cabaña.png">

{{% /section %}}

---

{{% section %}}
{{< slide background-image="/reveal-themes/transportation/bus-3.jpg" >}}

<div style="display: flex" class="row">
  <div style="flex: 45%" class="column"></div>
  <div style="flex: 55%" class="column">

  <h1 style="font-size:20rem;font-weight: bolder;color:#4AAF91"> 5 </h1>

  ### Línea L2-L3
  ### (Urbano Pozuelo)

  <div class="alert warning" style="background: #fef9c3;">
    <span>
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
           stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle">
           <path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path>
           <line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line>
      </svg>
    </span>
    <span>NOTA: Sección añadida a Posteriori</div>

  </div>

</div>

---

{{< slide background-image="/reveal-themes/transportation/text-6.jpg" auto-animate="" style="text-align: left;">}}

* 🤔 En mi opinión, esta doble línea circular está desaprovechada: ampliando sólo un minuto su recorrido, podría añadir conexión con Aravaca Cercanias, buses de la EMT y otra parada de Metro Ligero

* 🚂 Para ello, se propone un mínimo desvío con entrada en Aravaca, donde, con situar una nueva marquesina, se tendría muy facilitada la conexión con Cercanías y buses EMT

* ⏰ Aunque esta línea ya conecta con Cercanías Pozuelo, el paso antes por Aravaca permitiría adelantar la llegada al trabajo; además, conectaría la zona de Aravaca mejor con Pozuelo

* 🏍 Si, debido a la inclinación de la Calle del Cerro de Valdecahonde, fuera imposible que el bus pasase por allí, podría ir, en ida y vuelta, por Avenida del Talgo; aunque es menos óptimo.

---

{{< slide background-image="/reveal-themes/transportation/text-7.jpg">}}

<img src="L2 Horario.png">

---

{{< slide background-image="/reveal-themes/transportation/text-5.jpg">}}

<img src="L3 AntiHorario.png">

{{% /section %}}

---

{{< slide background-image="/reveal-themes/transportation/end.jpg">}}

</br></br></br></br>

# Gracias por su Atención!

[↩️](#) Vuelta al Inicio

</br></br></br></br>

<img style="position: absolute; left: 1000px; bottom: 0px;" data-src="/reveal-themes/creativecommons.png" width="20%">
