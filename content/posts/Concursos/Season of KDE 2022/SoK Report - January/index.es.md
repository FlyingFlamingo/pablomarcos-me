---
title: "SoK 2022 - Informe de Enero"
date: 2022-01-31
menu:
  sidebar:
    name: SoK 2022 Enero
    identifier: sokenero2022
    parent: seasonofkde2022es
    weight: 40
---

Al final, no logré participar en el SoK de 2022, pues estaba muy ocupado realizando el TFM; una lástima, otra vez será :p
