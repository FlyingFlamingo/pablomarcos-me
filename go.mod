module codeberg.org/flyingflamingo/pablomarcos-me

go 1.22.8

require (
	github.com/MarionMoseby/hugo-gallery-shortcode v0.0.0-20241130023409-789be4a9b9bc // indirect
	github.com/MarionMoseby/mastobutton v0.0.0-20241203012742-6fa7479e4062 // indirect
	github.com/hugo-toha/toha/v4 v4.7.0 // indirect
	github.com/joshed-io/reveal-hugo v0.0.0-20241030080325-e191f51d09be // indirect
	github.com/thisdev/hugo-gallery-shortcode v0.0.0-20241201183832-ab9d934ee574 // indirect
)
