---
title: "Merienda Barcelona - Octubre 2024"
date: 2024-10-01
menu:
  sidebar:
    name: Octubre 2024
    identifier: mb_octubre2024
    parent: meriendabcn
author:
  name: Merienda Barcelona
  image: /images/author/Logo Merienda Barcelona.png
---

Buenas noticias! Este 30 de Septiembre, se retoma el servicio de Merienda Barcelona, terminado el periodo estival y por tanto mis vacaciones!

## Semana del 30.09

Esta semana, con las prisas (ya que quería despedir a unos amigos que se iban de viaje a las 9!), hice el mismo Sandwich de Mozzarella, Pavo, Rúcula y Trufa que ya habréis visto. Aun así; ¡está buenísimo!

Además, fue una tarde muy enternecedora, pues los usuarios habituales estaban suuuper agradables (especialmente una Usuaria de una plaza, que me cae genial); y, además, ¡conocí a tres nuevos majísimos que espero volver a encontrarme!

{{< img src="/es/posts/concursos/merienda-barcelona/octubre-2024/30.09.jpeg" width="70%" align="center" title="Sandwich de Mozzarella, Pavo, Rúcula y Trufa; termo y botella vacía de leche de soja, así como magdalena y plátano en un mini-bowl" >}}

Finalmente, como la semana que viene no podré asistir (estaré en una cata de quesos para practicar el catalán 😋), avisé a los usuarios de que, en lugar del lunes, repetiré esta semana, el domingo!

# Semana del 07.10

Se me olvidó contar mi experiencia la semana pasada, así que, ¡aquí va!

Como iba con prisa, hice unos sándiches vegetales de huevo, atún y mahonesa que, a la plancha y con verdura, quedamos genial! De nuevo, me costó un poco repartirlos por mi zona, por lo que me desvié a visitar a los 3 usuarios que conocí la semana del 30.09. Además, empecé a llevar zumo de naranja para tener algo fresquito, y me he empezado a plantear si merecería la pena cambiar el café por caldo, ya que sólo tengo un termo 😅

{{< img src="/es/posts/concursos/merienda-barcelona/octubre-2024/06.10.jpeg" width="70%" align="center" title="Sandwich de Vegetal de Atún y Huevo; termo y botella vacía de leche de soja, así como magdalena, y naranja en sendos mini-bowls" >}}

# Semana del 14.10

Este lunes, no sabía muy bien qué hacer, y me apetecía algo más especial. Así, me puse manos a la obra, e hice dos DELICIOSAS tortillas de patata caseras (con cebolla, obviamente), que integré con lechuga y ali-oli en unos sándwiches con pintaza (ver fotos debajo). Además, como regalito especial, compré unas naranjitas recubiertas de chocolate; ¡que ricas!

Respecto a los usuarios, tuve el privilegio de servir a 11 personas, que, además, fueron ¡majísimos! Me siento muy privilegiado de poder ayudarles, y de poder compartir un rato de charleta con ellos, sean cuales sean sus variopintas opiniones. Especialmente encantador me parece un chico palestino que duerme en Francesc Maciá, y que es, honestamente, un amor :p

{{< img src="/es/posts/concursos/merienda-barcelona/octubre-2024/14.10.jpeg" width="70%" align="center" title="Sandwich de Tortilla de Patata, Ali-Oli y Lechuga; termo y botella vacía de leche de avena, así como napolitana de chocolate, uvas y naranja confitada y chocolateada en sendos mini-bowls" >}}

Estoy deseando que llegue el 21!

# Semana del 21.11

Esta semana, se me olvidó bloguear, porque estuve probando una inciativa que cuento en el siguiente titular... ya veréis qué chula!

{{< img src="/es/posts/concursos/merienda-barcelona/octubre-2024/21.10.jpeg" width="70%" align="center" title="Bagel de Trufa con Jamón York, Caldo de Verduras y Manzana" >}}

# Semana del 28.11

⚠️ Novedades en Merienda Barcelona!

Desde la semana pasada, he estado probando un nuevo proyecto piloto para ser 100% sostenibles, e incluso, negativos en carbono ♻️🌳

Y es que, desde el 21.11, además de ayudar a la gente sin 🏠,  Merienda Barcelona va a evitar el desperdicio de alimentos comprando en Too Good to Go!

🥐 En esta iniciativa, aprovecho esta app (que me encanta) para salvar comida del 365 de Muntaner. Así, me evito tener que cocinar bocadillos durante horas ⏰, y, al mismo tiempo, consigo una alternativa más sabrosa e igualmente saludable para los usuarios, sustituyendo la bollería industrial por otra de obrador, con bocadillos ricos y variados.

Además, para mantener la calidad nutricional de las meriendas, seguiré llevando fruta variada (por ejemplo, 🍊 este 28.11), acompañada de caldo calentito ☕ a petición de los usuarios.

Espero que esta iniciativa sirva para mejorar, en lo posible, la vida de la gente de la calle, reduciendo mis costes y mejorando los valores del proyecto 🎉

Nos vemos la semana del 11 del 11! :`p

{{< img src="/es/posts/concursos/merienda-barcelona/octubre-2024/28.10.jpeg" width="70%" align="center" title="Bandeja de Bollería en el 365, que ahora llevo en cada merienda!" >}}

---

📸 de Cabecera: [Gary Bendig](https://unsplash.com/es/@kris_ricepees) en [Unsplash](https://unsplash.com/es/fotos/fotografia-de-enfoque-selectivo-de-agapornis-amarillos-y-negros-lpzcjbzsUmI), [Unsplash License](https://unsplash.com/es/licencia)
