---
title: "Merienda Barcelona - Abril 2024"
date: 2024-03-25
menu:
  sidebar:
    name: Abril 2024
    identifier: mb_abril2024
    parent: meriendabcn
author:
  name: Merienda Barcelona
  image: /images/author/Logo Merienda Barcelona.png
---

Continúo con el diario de mis experiencias con el Proyecto Merienda Barcelona:

## Semana del 01.04

Esta semana, al ser el Lunes la [Pascua Florida](https://ajuntament.barcelona.cat/calendarifestius/es/), disponía de más tiempo para cocinar, por lo que decidí hacer algo un poco especial: El [Sandwich de Aguacate de Gipsy Chef](https://www.youtube.com/watch?v=IsLVm8sr-V8), y, de poste, [bartolillos](https://elpais.com/gastronomia/el-comidista/2024-03-19/bartolillos-el-dulce-madrileno-para-sobrellevar-la-semana-santa.html), un dulce típico de Madrid.

Lo que no pensé es que, al ser festivo, muchos de los usuarios no habían acudido a sus posiciones habituales, por lo que, el propio lunes, sólo pude atender a 2 usuarios y repartir 2 packs de merienda.

Siendo que me sentía super orgulloso de los sabores del menú, decidí probar suerte de nuevo el Martes, pudiendo repartir, por fin, los 5 kits de comida restantes a hasta 7 usuarios (ya que no todos querían de todo).

{{< img src="/es/posts/concursos/merienda-barcelona/abril-2024/01.04.jpeg" width="70%" align="center" title="Sandwich de aguacate, bartolillo y café." >}}

## Semana del 08.04

Este lunes, llevé algo más sencillo después de la debacle que supuso tirarme 3 horas cocinando bartolillos en semana santa: un sandwich de [la tortilla de patatas de gipsy chef](https://www.youtube.com/watch?v=fCe7IcBwNLI), un café con leche de arroz y, como cosa especial, un termo con leche para un usuario que me pidió. También llevé algunas cuchillas de afeitar (de las de seguridad, evidentemente), que me habían sobrado de la semana pasada.

De nuevo, fue un éxito en número de usuarios, y llegué a atender a 10 personas, teniendo que parar por el camino a comprar conchas de chocolate para que mis habituales no se quedasen sin comer, pues apareció más gente que solicitó comida antes de llegar con ellos. Cada día me sorprende lo necesario que es este servicio, y lo dejado que está por parte de la Administración.

En un punto positivo, ¡me encontré una rosa en la carretera! 💃

Así, recuerdo que, si alguien tiene tiempo por la zona de Sants en Barcelona, puede apuntarse a dar una vuelta!

{{< split 6 6>}}

![Café en termo, manzana en rodajas en platito, botella de leche de coco y arroz y sandwich tostado de tortilla de papas](/es/posts/concursos/merienda-barcelona/abril-2024/08.04.01.jpeg)

---

![Café con concha de chocolate y ¡rosa roja!](/es/posts/concursos/merienda-barcelona/abril-2024/08.04.02.jpeg)

{{< /split >}}

## Semana del 15.04

Esta semana, no pude acudir el Lunes, por lo que, tras avisar a los usuarios, cambié el día del reparto al Martes. Como tenía classes de Catalán, me tocó ir a la carrera, pero aún pude repartir 7 sandwiches y sus cafés, además de conchas de chocolate de acompañamiento (para reducir el tiempo de preparado). Los sandwiches también los hice rápido, de queso, huevo, lechuga y mayonesa, y con el pan sin tostar :+

Debido a algunas faltas, tuve que ampliar el recorrido, y dar algún sandwich después de clase, pero pude ayudar a más de 8 personas; ¡genial!

{{< img src="/es/posts/concursos/merienda-barcelona/abril-2024/15.04.jpeg" width="70%" align="center" title="Sandwich vegetal, dorayakis y café de leche de coco y arroz." >}}

## Semana del 22.04

Otra semana más que me tocó ir a toda prisa para no incumplir mis compromisos. Como, a las 18:30 del Lunes, tenía una [actividad para aprender catalán cocinando](https://www.cpnl.cat/xarxa/cnlbarcelona/cursos-de-catala/activitats/), y dado que el resto de días tenía otras cosas que hacer, opté por entregar los sándwiches los más rápido posible, llegando a atender a mis 8 usuarios habituales.

Preparé una variante del [Sandwich Chacarero del Comidista](https://www.youtube.com/watch?v=NGBrr3cGTmk&t=115s), con pavo en lugar de ternera y con pimiento en lugar de jalapeño para evitar el picante. Creo que quedó bastante bien! De acompañamiento, como siempre, llevé café con leche de arroz y coco, además de mandarinas y magdalenas del Mercadona :p

{{< img src="/es/posts/concursos/merienda-barcelona/abril-2024/22.04.jpeg" width="70%" align="center" title="Sandwich chacarero, magdalenas, mandarinas y café con leche." >}}

## Semana del 29.04

El lunes 29 llovió muchísimo, por lo que, para evitar que se echasen a perder los sándwiches ante la posible falta de usuarios, decidí pasar el día de reparto al martes. Elaboré el [Bocadillo de Burrata e Higos de Gypsy Chef](https://www.youtube.com/watch?v=5NeIyz-8NxA), pero con variaciones: pan de sandwich, queso de cabra y melón en lugar de higos. Me pareció una combinación maravillosa! (y mas barata jeje). Para acompañar, llevé madgalenas y fresas picadas, que, al estar de temporada, estaban más baratas y súper ricas.

De cara a la siguiente semana, avisé a los usuarios de que, para poder tomarme el tiempo de disfrutar y hablar con ellos, pasaría el día al 10/05 a las 17hrs, pues el lunes estaré en Madrid.

{{< img src="/es/posts/concursos/merienda-barcelona/abril-2024/30.04.jpeg" width="70%" align="center" title="Sandwich de queso de cabra y melón, fresas y café." >}}

---

📸 de Cabecera: [Theodor Vasile](https://unsplash.com/es/@theodorrr) en [Unsplash](https://unsplash.com/es/fotos/brwon-y-catedra-negra-LSscVPEyQpI), [Unsplash License](https://unsplash.com/es/licencia)
