# pablomarcos.me

<img src="https://codeberg.org/FlyingFlamingo/pablomarcos-me/raw/branch/main/screenshots/español.png" width="50%">.
<br>
<br>

-----

**EN**: This is the git repository for my personal site, [pablomarcos.me](pablomarcos.me). Only the HUGO source code is provided here, to generate the HTML:

1.  Clone this repository: `git clone https://codeberg.org/FlyingFlamingo/pablomarcos.me/`
2. Step into the directory: `cd pablomarcos.me`
3. To get the final HTML, deploy the site: `hugo -D`
4. To test any changes to the source using the live server: `hugo server -D`
5. The theme now uses hugo modules, so updates should be automatic! If they dont, try: `hugo mod get -u` or [see here](https://github.com/hugo-toha/toha#running-locally)

You also have access to the `deploy.sh` script: call it with `-s` or `--source` to upload the modified code to codeberg, or simply alone to update the website

-----

**ES**: Este es el repositorio git para mi sitio personal, [pablomarcos.me](pablomarcos.me). Aquí sólo se proporciona el código fuente de HUGO, para generar el HTML:

1.  Clona este repositorio: `git clone https://codeberg.org/FlyingFlamingo/pablomarcos.me/`
2. Entra en el directorio: `cd pablomarcos.me`
3. Para obtener el HTML, despliega el sitio: `hugo -D`
4. Para probar cualquier cambio en el código fuente usando el servidor en vivo: `hugo server -D`
5. El tema ahora usa módulos de hugo, por lo que se actualiza sólo! Si no, prueba: `hugo mod get -u` o [clica aquí](https://github.com/hugo-toha/toha#running-locally)

También tienes acceso al script `deploy.sh`: llámalo con `-s` o `--source` para subir el código modificado a codeberg, o solo para actualizar la web

-----

**FR**: Voici le dépôt git de mon site personnel, [pablomarcos.me](pablomarcos.me). Seul le code source HUGO est fourni ici, pour générer le HTML :

1. Cloner ce dépôt : `git clone https://codeberg.org/FlyingFlamingo/pablomarcos.me/`
2. Aller à le dossier : `cd pablomarcos.me`
3. Pour obtenir le HTML final, affichez le site : `hugo -D`
4. Pour tester tout changement dans le code source en utilisant le serveur en direct : `hugo server -D`
5. Le thème utilise des modules hugo, il se met donc à jour tout seul ! Si non, tappez: `hugo mod get -u` ou [cliquez ici](https://github.com/hugo-toha/toha#running-locally)

Vous avez également accès au script `deploy.sh` : appelez-le avec `-s` ou `--source` pour télécharger le code modifié sur codeberg, ou tout seul pour mettre à jour le site web.




