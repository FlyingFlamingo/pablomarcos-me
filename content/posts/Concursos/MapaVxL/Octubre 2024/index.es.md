---
title: "Actualització Octubre de 2024"
date: 2024-10-15
menu:
  sidebar:
    name: Octubre 2024
    identifier: mapavxl_octubre24
    parent: mapavxl
    weight: 40
---

{{< badge href="https://codeberg.org/FlyingFlamingo/mapa-vxl" badge="codeberg" >}}

</br>

Aquest Octubre de 2024, com cada 6 mesos, he actualitzat el mapa per a reflectir els canvis en establiments que pogués haver-hi. A més, per a obtenir alguns stats i aconseguir millores, he modificat el codi, que podeu veure en codeberg, i que ara inclou tests, checks que els punts estan correctament situats dins de Catalunya, i un comptador de nous elements!

Així, podem veure que, en aquests set mesos:

* 1294 comerços s'han sumat al Programa VxL
* 391 comerços s'han donat de baixa
* 2956 comerços s'han mantingut sense canvis

A més, 21 comerços que el script havia col·locat erròniament fora de Catalunya (possiblement a causa de noms incomplets o que manca) han estat eliminats.

Com sempre, espero que aquests canvis siguin útils a la comunitat del VxL, als Alumnes del CPNL i a internet en general. Com crec que aquest projecte és útil, i com veig que ha arribat a un nivell de maduresa acceptable, em plantejo enviar-l'hi a la gent del VxL i al meu professor de català, per a afavorir el seu discovery i més gent pot fer-ho servir.

<iframe width="100%" height="500px" frameborder="0" allowfullscreen allow="geolocation" src="https://framacarte.org/es/map/establiments-collaboradors-vxl_181936?scaleControl=false&miniMap=true&scrollWheelZoom=false&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=caption&captionBar=false&captionMenus=false#15/41.3847/2.1709"></iframe><p><a href="//framacarte.org/es/map/establiments-collaboradors-vxl_181936?scaleControl=false&miniMap=true&scrollWheelZoom=true&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=caption&captionBar=false&captionMenus=false#15/41.3847/2.1709">Ver pantalla completa</a></p>
