---
title: "Intro & 2023 - PCBakers"
date: 2023-12-31
menu:
  sidebar:
    name: Intro & 2023
    identifier: intro-2023
    parent: pcbakers
---

Cuando inicié mi trabajo en el IRB Barcelona, me enteré de la existencia de una organización benéfica, PCBakers, que organiza una venta de pasteles al mes para recaudar dinero para un proyecto solidario. Como quería mejorar mi repostería, y como me parece una fantasía de asociación, intenté colaborar lo posible dentro de mi estancia en el IRB / PCB; y, posteriormente, tras dejarlo, quise seguir colaborando, pues la idea me encantó.

## 21.06 - Inundaciones Cesena

Para mi primer Bake Sale, hice una tarta de queso fría estilo NY, de la que no tengo enlace a la receta pero que, por su abundancia, debería ser fácil encontrar alguno; creo que me quedó bastante rica, y eso que aún no tenía batidora para las mezclas! La cobertura fue de mermelada de fresas y gelatina

{{< img src="/es/posts/concursos/pcbakers/intro--2023/21.06.jpeg" width="70%" align="center" title="Tupper de cristal con tarta de queso con mermelada de fresa en una mano." >}}

## 12.07 - Malaria 40

Para esta Bake Sale, hice una [CheesCake en Air Fryer del NYTimes Cooking](https://cooking.nytimes.com/recipes/1022925-air-fryer-cheesecake). Como aún tenía miedo de que se me derritiera por el camino, la verdad es que me quedó congelada, pero le dimos un golpe de horno y listo.

También llevé al lab unas [Salted Margarita Bars](https://cooking.nytimes.com/recipes/1022342-salted-margarita-bars), que, si te gusta el tequila, están buenísimas, aunque tal vez algo fuertes

{{< img src="/es/posts/concursos/pcbakers/intro--2023/12.07.jpeg" width="70%" align="center" title="Meal-prep que incluye tarta de queso (amarilla, con la parte de arriba dorada) y tiras rectangulares de barras de tarta de margarita (el cóctel), también amarillentas y con base de galletas." >}}

## 21.09 - Fund. Josep Carreras

Como soy donante de médula convencido, a este Bake Sale no podía fallar. Así, con todo mi morro, me personé en el IRB días después de dejar el trabajo, y, además de entregar la tarta, informé todo lo que pude sobre el funcionamiento de la donación de médula; ¡como cuando estuve en el Equipo Médula UPM!

Respecto a lo que horneé, ni me acuerdo ni tengo foto, pero si que saqué una de una muffin muy cuqui que me compré:

{{< img src="/es/posts/concursos/pcbakers/intro--2023/21.09.jpeg" width="70%" align="center" title="Muffin con sprinkles de colorinchis" >}}


## 31.10 - Marruecos y Libia

Para este Bake Sale, que apoyaba a la Cruz Roja en su campaña de emergencia por los terremotos en el Norte de África, probé a elaborar el [Milhojas del Becario de El Comidista](https://www.youtube.com/watch?v=BSvSyWdcpbQ), que es súper fácil de hacer y súper resultón, aunque en la Air Fryer me costó un ratín.

Como era el cumple de una amiga, sustituí los higos por pistacho, que le encanta. Mirad qué pintaza!

{{< img src="/es/posts/concursos/pcbakers/intro--2023/31.10.jpeg" width="70%" align="center" title="Pastelitos de milhojas: sandwich de hojaldre relleno y cubierto de mascarpone, y con un poco de pistacho." >}}

## 29.11 - Gats Lliures Poblenou

Para esta, no tengo foto, pues la hice corriendo, y, al final, no pude llevarla. Me la tuve que comer con un amigo! Hice la [Apricot Upside-Down Cake del New York Times](https://cooking.nytimes.com/recipes/1021259-apricot-upside-down-cake)

El resto del año, entre la búsqueda del trabajo y otras cosas, no pude participar en más bake sales, pero me gustó mucho la experiencia, y decidí seguir incluso pese a no ser ya empleado PCB, incluso en 2024 💃

---

📸 de Cabecera: [Captura de Pantalla de la Web de PCBakers](https://willblev.github.io/PCBakers/), Abril 2024
