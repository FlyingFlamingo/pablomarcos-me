---
title: "Anuncio: Vacaciones de Verano 🌞"
date: 2024-07-01
menu:
  sidebar:
    name: Verano 2024
    identifier: mb_verano2024
    parent: meriendabcn
author:
  name: Merienda Barcelona
  image: /images/author/Logo Merienda Barcelona.png
---

Tras 27 semanas de servicio prácticamente ininterrumpido, este verano suspendemos el servicio para los usuarios por Vacaciones, hasta el 30 de Septiembre 🍂

Nos vemos entonces!

---

📸 de Cabecera: [Tim Mossholder](https://unsplash.com/es/@timmossholder) en [Unsplash](https://unsplash.com/es/fotos/senalizacion-unk-unk-en-blanco-y-negro-Ee-DnKZYmWU), [Unsplash License](https://unsplash.com/es/licencia)


