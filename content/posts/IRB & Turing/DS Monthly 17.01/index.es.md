---
title: "DS Monthly - 17.01"
author: Pablo Marcos
date: 2024-01-17
menu:
  sidebar:
    name: Presentación BMK
    identifier: ds_monthly_17_01
    parent: irb_turing_more
    weight: 40
---

{{< badge href="./Background Pablo - 17.01.2024 - NoLogo.pdf" badge="codeberg" >}} </br>

El 13 de Diciembre, fuí contratado en [Biome Makers](https://biomemakers.com/)! 🥳🥳

La verdad es que son gente majísima, y estoy encantado de poder formar parte de esta aventura. Como parte del Equipo de Data Science, trabajaré como Python Developer en su misión de optimizar las prácticas agroalimentarias, mejorar la salud del suelo, y ayudar a crear un mundo más sostenible, sano y con mayor seguridad alimentaria.

El 17 de enero, al cumplirse un mes de mi trabajo en la empresa, me pidieron que diera una presentación sobre mi background, de la que me siento muy orgulloso. **Una vez he eliminado toda información confidencial**, la dejo aquí:

---

{{< embed-pdf src="./Background Pablo - 17.01.2024 - NoLogo.pdf" >}}
