---
title: "Libertad"
date: 2023-09-27
menu:
  sidebar:
    name: Libertad
    identifier: libertad
    parent: poemas
    weight: 40
author:
  name: Mr Framework
  image: /images/author/Mr Framework.png
---

La libertad es un hechizo de oscuro significado, un fetiche colectivizado en cuyas nebulosas alturas se atoran, enzarzadas, las retóricas, como bolas de algodón que se desgarran en el espino de lo desconocido.

Y es que es, precisamente, sin esas espinas que no se puede entender la libertad. Nadie sabe muy bien qué quiere decir ser libre, pero sí sabemos cuando no lo somos.

Sabemos cuando nos duele el alma, cuando nos pesa el pecho derrumbando la columna partida de un corazón roto; o, en mi caso, cando me explota la cabeza, presionada por un cerebro escapista que guerrea por huir en globo.

Incluso cuando estamos en paz, no sabemos muy bien dónde queda la libertad; si a la orilla de un lago en calma, o en la fuerza de nuestro aliento para mover en él tormentas.

Y es que, ya sea la libertad un vaporoso sueño inalcanzable, un hechizo que nos maldice con su locura, o una realidad que nos espera al otro lado del espejo, sólo hay una forma de saberlo:

Saltando.

---

📸 :  [Timur Kozmenko](https://unsplash.com/es/@timrael) on [Unsplash](https://unsplash.com/es/fotos/QZUY7c0eIdw)

#poesia #poetry #libertad #freedom #poesiaenespañol

{{<mastobutton>}}
