---
title: "Merienda Barcelona - Junio 2024"
date: 2024-05-27
menu:
  sidebar:
    name: Junio 2024
    identifier: mb_junio2024
    parent: meriendabcn
author:
  name: Merienda Barcelona
  image: /images/author/Logo Merienda Barcelona.png
---

Continúo con el diario de mis experiencias con el Proyecto Merienda Barcelona:

## Semana del 03.06

Este mes de Junio, ha sido la primera semana que me he saltado desde que empecé este proyecto. Muy a mi pesar, tieniendo el lunes ocupado y el viernes con cita para donar sangre (otro tipo de voluntariado jeje), avisé a los usuarios, no pudiendo hacer mucho más.

## Semana del 10.06

Esta semana, realicé un bocadillo casero con cosas que quería gastar de la nevera: pepino, cherries, aceitunas negras y queso vegano marca Väcka que pedí para la ocasión en Too Good to Go. Para acompañar, como siempre, llevé un café, esta vez con leche de coco, un dorayaki, y manzana picada.

El servicio fue bien, y, salvo un pequeño drama médico no relacionado con el mismo, no hubo nada reseñable, pudiendo ayudar a unos 10 usuarios.

{{< img src="/es/posts/concursos/merienda-barcelona/junio-2024/10.06.jpeg" width="70%" align="center" title="Sandwich de pepino, cherries, aceitunas y queso vegano, con dorayaki y manzana." >}}

## Semana del 17.06

Para ampliar la variedad de los alimentos que traigo, elaboré [el Banh Mi del Gipsy Chef](https://www.lavanguardia.com/comer/recetas/20190320/461133623337/receta-bocadillo-vietnamita-gipsy-chef-banh-mi-video-seo-lv.html
), na receta muy original con encurtidos que creo que me quedó bastante rica. Para acompañarlo, hubo, de nuevo, café con lechde de coco, dorayaki y manzana.

Tuve unos 8 usuarios, más una chica "extra" a la que conocí de fiesta y a quien me acerqué para tener un detalle.

{{< img src="/es/posts/concursos/merienda-barcelona/junio-2024/17.06.jpeg" width="70%" align="center" title="Sandwich banh mi, con dorayaki, café y manzana." >}}

## Semana del 25.06

Al ser el Día de San Juan festivo en Barcelona, y, no sabiendo si tendría problemas o no, avisé a los usuarios de que, esta semana, se moverían los servicios al Martes 25, especialmente ahora que mis clases de Catalán se han terminado y tengo más tiempo. Como me apetecía ser original, hice un sandwich de tortilla francesa con cebollino, beicon, alioli y cebolla caramelizada. Me parece que me quedó riquísimo!

Para acompañarlo, además del café habitual, puse unas magdalenas valencianas y plátano canario, manteniendo la intención de llevar siempre una pieza de fruta para mejorar la calidad nutricional de las meriendas. Además, conocí a un nuevo usuario madrileño (como yo!) y me lo pasé bastante bien hablando con él. La gente me va cogiendo confianza y cariño, y eso mola!

{{< img src="/es/posts/concursos/merienda-barcelona/junio-2024/25.06.jpeg" width="70%" align="center" title="Entrepán de tortilla francesa con beicon, cebolla caramelizada y ali-oli; mandalena valenciana, capuchino y plátano." >}}


---

📸 de Cabecera: [Elaine Casap](https://unsplash.com/es/@ecasap) en [Unsplash](https://unsplash.com/es/fotos/cuenco-de-tomates-servido-en-la-mano-de-la-persona-qgHGDbbSNm8), [Unsplash License](https://unsplash.com/es/licencia)

