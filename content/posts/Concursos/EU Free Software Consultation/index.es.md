---
title: "EU Free Software Foundation"
date: 2024-09-16
menu:
  sidebar:
    name: EU Free Software
    identifier: eufreesoftwareconsultation
    parent: concursos
    weight: 40
---

Gracias a [la newsletter de la Free Software Foundation Europe](https://fsfe.org/news/2024/news-20240911-01.es.html), y a [un post en Lemmy](https://lemmy.ml/post/18094546), llevaba tiempo sabiendo que la Unión Europea planeaba eliminar 27 millones de euros de financiación para Software Libre, como las becas NGI Zero que se han usado para desarollar gran parte del software que permite que el Fediverso funcione.

Como usuario de Mastodon, esta noticia me apena bastante, así que, en cuanto he descubierto que [la Consulta Ciudadana sobre este recorte sigue abierta](https://ec.europa.eu/info/law/better-regulation/have-your-say/initiatives/13880-Digital-Europe-programme-interim-evaluation/public-consultation_en), no he podido evitar dar mi opinión.

Os recomiendo que hagáis lo mismo!

URL: https://ec.europa.eu/info/law/better-regulation/have-your-say/initiatives/13880-Digital-Europe-programme-interim-evaluation/public-consultation_en

---

📸 : [Christian Lue](https://unsplash.com/es/@christianlue) on [Unsplash](https://unsplash.com/es/fotos/bandera-azul-en-la-parte-superior-del-edificio-durante-el-dia-MZWBMNP7Nro); [Unsplash License](https://unsplash.com/es/licencia)

{{<mastobutton>}}

