---
title: "CanGraph Update"
author: Pablo Marcos
date: 2022-11-14
output: pdf_document
menu:
  sidebar:
    name: CanGraph Update
    identifier: cangraph_update
    parent: tfm
    weight: 40
---

For a brief update on my work, and to showcase the usefulness of the software I am creating, the 15th of November, 2022, I gave a presentation based on the following slides during IARC's OncoMetaBolomics Team's Monthly Branch Meeting.

<div class="fluidMedia">
    <iframe src="/es/presentations/cangraph-update" frameborder="0"> </iframe>
</div>


<style>

.fluidMedia {
    position: relative;
    padding-bottom: 56.25%; /* proportion value to aspect ratio 16:9 (9 / 16 = 0.5625 or 56.25%) */
    height: 0;
    overflow: hidden;
}

.fluidMedia iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

</style>
