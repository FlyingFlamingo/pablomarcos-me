---
title: "Merienda Barcelona - Julio 2024"
date: 2024-07-01
menu:
  sidebar:
    name: Julio 2024
    identifier: mb_julio2024
    parent: meriendabcn
author:
  name: Merienda Barcelona
  image: /images/author/Logo Merienda Barcelona.png
---

Junio es el último mes de la temporada en el que podré hacer voluntariado, ya que colgaré el cartel hasta Septiembre; así, ¡procedo a contar qué tal va el mes!

## Semana del 01.07

Esta semana, como había quedado a las 10 para ir al cine, me pasé el voluntariado como Mario con la ⭐: A toda leche jeje

De plato principal, tenemos [un sandwich de hummus de pimientos de El Comidista](https://www.youtube.com/watch?v=NGBrr3cGTmk&t=40s), que lleva tapenade casera de olivas y rúcula, además de mozzarella (mi toque personal) para hacerlo más jugoso. De acompañamiento, puse, de nuevo, dorayaki, café y, de fruta, fresa picada, que tiene muy buena aceptación entre los usuarios, quienes incluso a veces me lo han solicitado.

{{< img src="/es/posts/concursos/merienda-barcelona/julio-2024/01.07.jpeg" width="70%" align="center" title="Bocadillo de Hummus y Tapenade con Pimientos; termo y botella vacía de leche de coco, así como dorayaki y fresas picadas en un mini-bowl" >}}

## Semana del 08.07

Última quincena de este proyecto de Voluntariado! 💃

El 8 de Julio, hice un [Bocadillo de Reina Pepiada](https://www.youtube.com/watch?v=NGBrr3cGTmk&t=184s), una receta #venezolana que conocí en @elcomidista y que -cómo podía ser de otra forma- está ¡buenísima!

Tuve la oportunidad de ayudar a 9 usuarios, entre ellos, los habituales, una señora neerlandesa super maja y elegante 💅🏻, y un argelino que hablaba alemán! 🤯 Menos mal que sé varios idiomas, porque a veces esto del voluntariado es una LOCURA donde se mezclan todos los idiomas de la tierra! 🤣

{{< img src="/es/posts/concursos/merienda-barcelona/julio-2024/08.07.jpeg" width="70%" align="center" title="Bocadillo de Reina Pepiada (crema de aguacate, mahonesa y pollo); magdalenas valencias, plátano, y termo de café con leche de almendras." >}}

## Semana del 15.07

Esta semana, del 15 de Julio, he querido hacer una despedida a lo grande, y he hecho las [Patty Melt del Torpedo](https://www.youtube.com/watch?v=fvcBFCQQUzI&t=45s), también de @elcomidista, y que quedan riquísimas (aunque el pan puede deshacerse... 😅😅)

Para acompañarlo, y ya que era el último día en MESES 📅, he comprado un regalito a cada usuario: golosinas rumanas, cuadernos y bolis, comida para los perritos... de todo un poco! Además, he vuelto a hacer Tarta de Santiago, que me encanta porque se tarda dos minutos de reloj y sale riquísima siempre 🍰🌠

La verdad es que ha sido un éxito, y uno de los días más emotivos y en los que más he hablado; casi tres horas en total sin parar! ⏰ Incluso, he conocido a un grupo de chicos peruanos súper majos con los que he intercambiado algo de conversación 😊

Con esto, colgamos el cartel de cerrado hasta Septiembre. Nos vemos pronto!

{{< img src="/es/posts/concursos/merienda-barcelona/julio-2024/15.07.jpeg" width="70%" align="center" title="Patty Melt (hamburguesa en pan de molde), con zumo de naranja, tarta de santiago, plátano canario, magdalena redonda, café, y varios regalos: chocolates rellenos de Lotus y Oreo, chocolatinas rumanas, colonia, y comida para perros" >}}

---

📸 de Cabecera: [Taisia Karaseva](https://unsplash.com/es/@taisiat) en [Unsplash](https://unsplash.com/es/fotos/una-vista-de-la-ciudad-desde-lo-alto-de-un-edificio-X4wcsMoc73o), [Unsplash License](https://unsplash.com/es/licencia)


