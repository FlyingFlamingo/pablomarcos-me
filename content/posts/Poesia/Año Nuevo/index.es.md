---
title: "Año Nuevo"
date: 2023-01-01
menu:
  sidebar:
    name: Año Nuevo
    identifier: ano_nuevo
    parent: poemas
    weight: 40
author:
  name: Mr Framework
  image: /images/author/Mr Framework.png
---

Ya va sonando el carrillón. En esos breves segundos, en esos instantes en los que tiemblan mis manos hambrientas de sueños, el año nuevo que se vislumbra, lejano aún en el horizonte, parece abalanzarse poco a poco sobre nosotros, como la pesada página de un libro incunable que ve por fin la luz del día.

Y así, una a una, voy engullendo las uvas, como doce musas consejeras traídas del más largo año, los doce últimos (¿o eran los primeros?) compases de un vals que se pierde en la lejanía. Como el año, empieza fácil la tarea, y jugueteo con la primera en la lengua como quien baila un tango: intenso pero relajado. Para la quinta, el baile se ha convertido en una jota, podeída mi boca por la voz del chicote, que ordena comer. Nueve, diez, once... cuando llego ya a la última, parecen mis mandíbulas una clase de zumba, y mis dientes, los exhaustos alumnos de un año tedioso pero lleno de dulzor.

Y, entonces, ya está hecho. Con el estruendo de cien mil fuegos artificiales, ha pasado por fin la página, y comienza un nuevo capítulo, una temporada completa de ese libro en blanco que es el futuro. Un capítulo lleno de páginas por descubrir, de bailarinas de papel que algunos llenaremos de historias y que otros preferirán convertir en flores, en gaviotas o en barquitos con los que surcar la tempestad que les nubla. Un año, dicho sea lo evidente, lleno de esperanza, de propósitos que se olvidan porque también evolucionan, de mil luces y cien sombras que llenen de sal nuestra vida. Un año lleno de sueños, de deseos para lo propio y para lo ajeno, una nueva oportunidad de brillar con luz propia.

Y, mientras todos se emperifollan para celebrar y yo me preparo para dormir tranquilo, lo tengo claro: hoy empieza mi nueva vida, hoy no recordaré más el pasado.

---

📸 :  [Kateryna Hliznitsova](https://unsplash.com/es/@kate_gliz) on [Unsplash](https://unsplash.com/es/fotos/ocb4ft1qyjA)

#poesia #poetry #añonuevo #newyear #poesiaenespañol

{{<mastobutton>}}
