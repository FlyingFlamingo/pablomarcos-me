# section information
section:
  name: Projets
  id: projects
  enable: true
  weight: 5
  showOnNavbar: true
  # Can optionally hide the title in sections
  # hideTitle: true

# filter buttons
buttons:
- name: Tous
  filter: "all"
- name:  Professionnel
  filter: "professional"
- name: Académique
  filter: "academic"
- name: Hobby
  filter: "hobby"
- name: Bénévolat
  filter: "volunteering"

# your projects
projects:
- name: Mise à jour du Chemical Checker
  logo: /images/sections/fediverse.png
  role: Assistant de Recherche
  timeline: "Janvier 2022 - Présent"
  url: "https://www.chemicakchecker.org"
  summary: En utilisant une grande variété d'outils, j'ai mis à jour, maintenu et réparé les ressources web du groupe SB&NB, y compris [le Chemical Checker](https://www.nature.com/articles/s41587-020-0502-7), un signaturiseur de bioactivité très complexe, et [Interactome3D](https://www.nature.com/articles/nmeth.2289).
  tags: ["academic","professional","graphs"]

- name: Travail chez Turing BioSystems
  logo: /images/sections/turing.jpg
  role: Stagiaire
  timeline: "Février 2023 - Mai 2023"
  url: "https://thecreatorfund.com/case-studies/turing-biosystems/"
  summary: En me basant sur le travail effectué à l'IARC, j'ai été embauché pour fournir des données provenant de multiples bases de données à la Plateforme d'IA Interprétable de Turing.Bio, leur permettant d'étudier les mécanismes de la maladie et de fournir de nouvelles solutions à des problèmes existants, en collaboration avec l'académie.
  tags: ["academic","professional", "graphs"]

- name: Investigation du Cancer à l'aide de Bases de Données de Graphes
  logo: /images/sections/neo4j.jpg
  role: Enquêteur
  timeline: "Janvier 2022 - Février 2023"
  url: "files/Memoria TFM FIRMADA.pdf"
  summary: Ma [thèse de master](https://oa.upm.es/76619/), réalisée à l'Agence Internationale de Recherche sur le Cancer, utilise des bases de données de graphs pour trouver des associations potentielles avec le cancer dans les composants nouvellement découverts ou déjà existants du métabolome humain. [Nous l'avons appelé CanGraph](https://flyingflamingo.codeberg.page/cangraph/).
  tags: ["academic","biotech", "graphs"]

- name: Découverte de Plomb pour l'Alopécie Androgénétique
  logo: /images/sections/5aR_typeII.jpeg
  role: Enquêteur
  timeline: "Mars 2022 - Avril 2022"
  url: "files/Using_Computational_Methods_to_Discover_Novel_Drugs_for_the_Treatment_of_Androgenetic_Alopecia.pdf"
  summary: Un article étudiant le potentiel des méthodes computationnelles pour développer de nouveaux médicaments pour le traitement de l'alopécie androgénétique. Il a été publié par le [Congrès des Étudiants en Biotechnologie](https://oa.upm.es/70568/)
  tags: ["academic","lead discovery","biotech"]

- name: Tacrolimus et transplantation rénale
  logo: /images/sections/chu-grenoble.png
  role: Chercheur-Programmeur
  timeline: "Avril 2021 - Juillet 2021"
  url: "files/Memoria TFG FIRMADA.pdf"
  summary: La thèse de mon diplôme analyse l'effet des trajectoires des niveaux sanguins résiduels de tacrolimus sur la survie des allogreffes rénales. Le code peut être [consulté ici](https://codeberg.org/FlyingFlamingo/destination-grenoble), et le texte est publié en [accès ouvert dans le référentiel de l'UPM](https://oa.upm.es/69676/).
  tags: ["academic","biostatistics","biotech"]

- name: Cambia tu Mundo
  role: Bénévole
  timeline: "Janvier 2022 - Juin 2022"
  logo: /images/sections/exe.jpeg
  summary: J'ai travaillé en tant que bénévole dans un projet d'apprentissage par le service dans les écoles, consistant à offrir un encadrement technologique pour encourager les vocations STEM chez les élèves.
  url: "files/CambiaTuMundo.pdf"
  tags: ["volunteering"]

- name: Who's That Function
  logo: /images/sections/godot.png
  role: Développeur de Jeux
  timeline: "Juin 2020 - Septembre 2020"
  url: https://codeberg.org/FlyingFlamingo/Whos-That-Function # Si votre projet est un référentiel public sur GitHub, fournissez ce lien. il affichera le nombre d'étoiles.
  #repo: ""  # Si votre projet n'est pas un référentiel public mais qu'il a un site web ou une URL vers des détails externes, fournissez-le ici. ne fournissez pas simultanément "repo" et "url".
  summary: Un jeu similaire à "Qui est-ce ?" pour montrer aux élèves les propriétés fondamentales des fonctions. [En espagnol]
  tags: ["professional", "Godot Engine", "GDScript"]

- name: Journal de la Pensée Mathématique
  logo: /images/sections/wordpress.png
  role: Développeur Web
  timeline: "Janvier 2021 - Mars 2021"
  url: https://revista.giepm.com/en/ # Si votre projet est un référentiel public sur GitHub, fournissez ce lien. il affichera le nombre d'étoiles.
  #repo: ""  # Si votre projet n'est pas un référentiel public mais qu'il a un site web ou une URL vers des détails externes, fournissez-le ici. ne fournissez pas simultanément "repo" et "url".
  summary: J'ai modernisé le site web du [Journal MT](https://revista.giepm.com/), et celui du [Groupe d'Innovation Éducative "Pensée Mathématique"](https://giepm.com/), en utilisant WordPress
  tags: ["professional", "WordPress", "web development"]

- name: Saison de KDE 2021
  logo: /images/sections/kde-logo.png
  role: Développeur Web
  timeline: "Janvier 2021 - Avril 2021"
  url: https://www.pablomarcos.me/posts/concursos/season-of-kde-2021/sok-final-status-report/ # Si votre projet est un référentiel public sur GitHub, fournissez ce lien. il affichera le nombre d'étoiles.
  #repo: ""  # Si votre projet n'est pas un référentiel public mais qu'il a un site web ou une URL vers des détails externes, fournissez-le ici. ne fournissez pas simultanément "repo" et "url".
  summary: J'ai contribué à moderniser le site web d'Okular, le lecteur de documents universel de KDE, en utilisant le thème aether-sass de Carl Schwan.
  tags: ["professional", "HUGO", "web development", "html5"]

- name: Modélisation de la Covid en utilisant Python
  logo: /images/sections/coronavirus.png
  role: Écrivain
  timeline: "Juin 2020"
  #repo: https://github.com/tensorflow/tensorflow
  url: "files/ Alóndiga_Marcos_Patón_Estudio_de_la_Pandemia_por_CoViD-19_mediante_modelos_compartimentales.pdf"
  summary: Un article mesurant l'impact des mesures de restriction des déplacements lors de la première vague de la CoViD-19 en Espagne [En espagnol]. Il a été publié par le [Congrès des Étudiants en Biotechnologie](http://oa.upm.es/67410/)
  tags: ["academic", "mathematical models","biotech"]

- name: Conseil de Représentants @ UPM
  logo: /images/sections/ETSIAAB.png
  role: Représentant
  timeline: "Septembre 2018 - Présent"
  #repo: https://github.com/hossainemruz/toha
  summary: Je fais partie du Conseil des Représentants des Étudiants de mon université et, à ce titre, du Conseil de Gestion du Département de Biotechnologie et de Biologie Végétale, ce qui m'a aidé à apprendre comment fonctionnent les grandes organisations et, par exemple, comment les budgets sont approuvés.
  tags: ["academic"]

- name: Wikipédia
  logo: /images/sections/wikipedia.png
  role: Contributeur
  timeline: "Mai 2015 - Présent"
  #repo: https://github.com/hossainemruz/toha
  summary: Je contribue à Wikipédia, le corpus collectif de la connaissance de l'Humanité
  url: https://es.wikipedia.org/wiki/Usuario:Marion_Moseby
  tags: ["hobby","open source"]

- name: AULA
  role: Bénévole
  timeline: "Mai 2019"
  logo: /images/sections/aula.jpg
  summary: J'ai été bénévole pour AULA, une Foire Internationale où j'ai aidé à informer les élèves sur les possibilités de carrière dans les domaines des biosciences et des sciences vertes.
  tags: ["volunteering"]

- name: Reparto Solidario Universitario
  role: Bénévole
  timeline: "2017-2020"
  logo: /images/sections/RSU.jpg
  summary: Pendant plus de trois ans, j'ai été bénévole pour le Reparto Solidario Universitario (RSU), une activité reconnue à l'UPM où nous aidons les sans-abri en leur apportant de la nourriture et de la compagnie.
  url: https://www.instagram.com/rsu_madrid/?hl=es
  tags: ["volunteering"]

- name: OSM
  role: Contributeur
  timeline: "2017 - Présent"
  logo: /images/sections/osm.png
  summary: Je contribue à OSM, la plus grande base de données ouverte pour les données géospatiales
  tags: ["open source", "hobby"]

- name: Conférence sur le Changement Climatique COP25
  role: Bénévole
  timeline: "2019"
  logo: /images/sections/COP25.png
  summary: J'ai été bénévole au sommet COP25 sur le changement climatique à Madrid, où j'ai appris de première main les problèmes de notre époque et comment ils peuvent être résolus avec l'aide de la science. J'ai également pu voir comment les grands événements sont gérés et organisés, de première main.
  url: "files/COP25 Certificate.pdf"
  tags: ["volunteering"]

- name: BOINC
  role: Bénévole
  timeline: "2016 - Présent"
  logo: /images/sections/boinc.jpg
  url: "/files/BOINC.pdf"
  summary: Je contribue la puissance de calcul inutilisée de mes appareils au réseau BOINC.
  tags: ["volunteering"]

