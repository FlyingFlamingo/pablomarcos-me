---
title: "Group Meeting - 05.07"
author: Pablo Marcos
date: 2023-07-05
menu:
  sidebar:
    name: Primer Group Meeting IRB
    identifier: group_meeting_05_07
    parent: irb_turing_more
    weight: 40
---

Como parte del trabajo en el IRB, cada cierto tiempo todos los miembros del Grupo damos una sesión (llamada *Group Meeting*) donde detallamos nuestro trabajo desde la última vez que lo hicimos. A mi me tocó por primera vez el 05.07, tras un mes actualizando el Chemical Checker, un *bioactivity signaturizer* que dispone de recurso web en [https://chemicalchecker.org/](https://chemicalchecker.org/). El progreso, del que me siento muy orgulloso, puede verse a continuación:

---

{{< embed-pdf src="/es/posts/irb--turing/group-meeting-05.07/Pablo's Group Meeting 05.07.pdf" >}}
