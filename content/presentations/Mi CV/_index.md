---
title: CV - Pablo Marcos
subtitle: Curriculum Vitae
outputs: Reveal
reveal_hugo:
  custom_theme: /reveal-themes/scientist/styles.css
  margin: 0.2
  width: 1200
  height: 675
  slide_number: true
  highlight_theme: color-brewer
  transition: slide
  transition_speed: medium
  templates:
    hotpink:
      class: hotpink
      background: '#FF4081'

---

{{< slide background-image="/reveal-themes/scientist/image-5.jpg" >}}

</br></br>

<div style="display: flex" class="row">
  <div style="flex: 30%" class="column">
    <img style="margin-right: 30%;" data-src="me.png" width="65%">
  </div>
  <div style="flex: 60%" class="column">

  </br> </br>

  # Curriculum Vitae

  ~ by [Pablo Ignacio Marcos López](https://www.pablomarcos.me/)

  </div>
</div>

</br></br>

---

{{% section %}}
{{< slide background-image="/reveal-themes/scientist/humaan-1.jpg" >}}

<div style="display: flex" class="row">
  <div style="flex: 45%" class="column"></div>
  <div style="flex: 55%" class="column">

  <h1 style="font-size:20rem;font-weight: bolder;color:#402664FF"> 1 </h1>

  ### Academic Background
  </div>
</div>

---

{{< slide background-image="/reveal-themes/scientist/text-5.jpg" >}}

## BSc: Biotechnology UP Madrid

</br>
<div style="display: flex;" width="90%">
  <div style="flex: 30%">
    <img data-src="ETSIAAB.jpg" width="100%">
    <small>UPM's ETSIAAB, where I pursued my Bachelor's in Biotechnology. </br> CC BY-SA 3.0 <a href="https://commons.wikimedia.org/wiki/File:ETSI_Agr%C3%B3nomos-UPM.jpg">Carlos Delgado</a></small>
  </div>
  <div style="flex: 5%"></div>
    <div style="flex: 70%">
       <ul>
        <li class="fragment">🗓️ From 2017 to 2021</li>
        <li class="fragment">🥇 One of the best Engineering Schools in Spain</li>
        <li class="fragment">🤔 I learnt a lot about the biology of living beings, from the cell to the organism; how to work in wet and dry lab settings; and how to create innovative solutions to existing problems</li>
        <li class="fragment">💡 I discovered an interest in bioinformatics</li>
        <li class="fragment">💼 Took part in an ERASMUS @ INSA Lyon!</li>
      </ul>
  </div>
</div>
<div style="display: flex;" width="90%">
<div style="flex: 30%" class="fragment"></br></br><small>Me recreating a Silver Mirror in a experiment and transplanting plants from a meristem</small></div>
<div style="flex: 40%" class="fragment"></br><img data-src="https://pic.infini.fr/d1qHXR2E/Jck3m0JE.jpg" width="45%"></div>
<div style="flex: 40%" class="fragment"></br><img data-src="meristemos.jpeg" width="70%"></div>
</div>

---

{{< slide background-image="/reveal-themes/scientist/text-2.jpg" >}}

## MSc: Computational Biology

</br>
<div style="display: flex;" width="90%">
  <div style="flex: 5%"></div>
    <div style="flex: 70%">
       <ul>
        <li class="fragment">🗓️ From 2021 to 2022</li>
        <li class="fragment">❓ I wanted to specialize in bioinformatics</li>
        <li class="fragment">👨‍🏫 I learnt python, R, ruby and BaSH; learnt about evolutionary biology and mathematics; and was shown how to work with RNASeq and NGS data</li>
        <li class="fragment">💡 I discovered an interest in bioinformatics</li>
        <li class="fragment">🔄 Moved back to Lyon for my internship!</li>
      </ul>
  </div>
  <div style="flex: 30%">
    <img data-src="montegancedo.jpg" width="100%">
    <small>UPM's "Campus de Montegancedo", in the middle of the bosque. </br> CC BY-SA 3.0 <a href="https://commons.wikimedia.org/wiki/File:ESA-USOC--02.jpg">Santiago Urueña</a></small>
  </div>

</div>
<div style="display: flex;" width="90%">
<div style="flex: 30%" class="fragment"></br></br><small>🔎 To help next year's students, I uploaded my works to my website. It got more than 6000 yearly views!</small></div>
<div style="flex: 70%" class="fragment"></br><img data-src="website.png" width="50%"></div>
</div>

{{% /section %}}

---

{{% section %}}
{{< slide background-image="/reveal-themes/scientist/humaan-4.jpg" >}}

<div style="display: flex" class="row">
  <div style="flex: 55%" class="column">

  <h1 style="font-size:20rem;font-weight: bolder;color:#402664FF"> 2 </h1>

  ### Research Experience
  </div>
  <div style="flex: 45%" class="column"></div>
</div>

---

{{< slide background-image="/reveal-themes/scientist/text-3.jpg" >}}

## Internship @ CHU Grenoble

<ul>
  <li class="fragment">🗓️ From April to July 2021</li>
  <li class="fragment">🥅 We wanted to know if the <strong>trajectories</strong> of tacrolimus blood level concentration affected kidney allograft survival </li>
  <li class="fragment">🐍 We used python, R and automatic clustering based on Dynamic Time Warping to determine 3 patient groups</li>
  <li class="fragment">😥 Unfortunately, no link to survival was found :(</li>
</ul>

</br>
<img class="fragment" style="width:65%" data-src="tacrolimus.png" width="20%">


---

{{< slide background-image="/reveal-themes/scientist/text-6.jpg" >}}

## Researcher @ IARC / WHO

</br>

<div style="display: flex" class="row">
  <div style="flex: 75%" class="column">
    <ul>
      <li class="fragment">🗓️ From February 2022 to January 2023</li>
      <li class="fragment">🌐 We developed CanGraph, a python utility to study and analyse cancer-associated metabolites using knowledge graphs </li>
      <li class="fragment">📥 When given a query, the program searches 7 existing, high quality databases, and returns a GraphML export of related information</li>
      <li class="fragment">📜 We want to publish the results, which led to my Master's Thesis, to a big Journal, such as Bioinformatics or Scientific Reports</li>
    </ul>
    </br>

  </div>
  <div style="flex: 25%" class="column">
   <iframe data-src="https://omb-iarc.github.io/CanGraph/" title="CanGraph's Documentation" height="500px" width="300px" ></iframe>
  </div>
</div>

---

{{< slide background-image="/reveal-themes/scientist/text-7.jpg" >}}

## Internship @ Turing Biosystems

</br>

<ul>
    <li class="fragment">🗓️ From February 2023 to May 2023</li>
    <li class="fragment">At Turing Bio, I worked in the intersection of Academia 🧪 and Enterprise 🏬 to elect, optimize, and design new therapies based on existing data </li>
    <li class="fragment">👀 Based on the work done at IARC, I transformed databases to provide as input to Turing's Interpretable AI Platform</li>
    <li class="fragment">🦁 I left Lyon in March 2023 (after 3 years!) to pursue a Job in IRB Barcelona</li>
</ul>

---

{{< slide background-image="/reveal-themes/scientist/text-4.jpg" >}}

## Researcher @ Institut de Recerca Biomèdica

<div style="display: flex;" width="90%">
  <div style="flex: 5%"></div>
    <div style="flex: 70%">
       <ul>
        <li class="fragment">😎 My Current Job!</li>
        <li class="fragment">❓ I wanted to come back to Spain to work in bioinformatics, both of which I love 💞</li>
        <li class="fragment">🦾 Using my ample experience, I worked on updating the Chemical Checker, a really complex bioactivity signaturizer with a web resource</li>
        <li class="fragment">🌐 I also am also keeping other of the Group's Websites up to date, such as <a href="https://interactome3d.irbbarcelona.org/">Interactome3D</a> (part of the Elixir network) and <a href="https://dsysmap.irbbarcelona.org/">DSysMap</a> </li>
      </ul>
  </div>
  <div style="flex: 30%">
    <img data-src="chemicalchecker.png" width="100%">
    <small> <a href="https://chemicalchecker.org">The Chemical Checker's Website</a> </br>  <a href="https://gitlabsbnb.irbbarcelona.org/packages/chemical_checker/-/blob/master/LICENSE">MIT License</a></small>
  </div>

---

{{< slide background-image="/reveal-themes/scientist/text-1.jpg" >}}

## Additional Experience

<ul>
    <li class="fragment">In collaboration with fellow students, we performed a modeling of the <a href="https://oa.upm.es/67410/">effects of confinement during the first wave of the CoViD-19 pandemic</a> that was published in the Proceedings Book of the <strong>2021 Biotechnology Student Congress</strong>, which attests to its quality</li>
    <li class="fragment"> While on my Master's, I published a paper on <a href="https://oa.upm.es/70568/">Computational Lead Discovery for Androgenetic Alopecia</a> at the <strong>2022 Biotechnology Student Congress</strong></li>
</ul>

{{% /section %}}

---

{{% section %}}
{{< slide background-image="/reveal-themes/scientist/humaan-2.jpg" >}}

<div style="display: flex" class="row">
  <div style="flex: 45%" class="column"></div>
  <div style="flex: 55%" class="column">

  <h1 style="font-size:20rem;font-weight: bolder;color:#402664FF"> 3 </h1>

  ### Motivation

  </div>

</div>

---

{{< slide background-image="/reveal-themes/scientist/text-6.jpg" transition="concave" auto-animate="">}}

# Improving my Professional Career

<li>👨‍🏫 Having finished my Master's, a PhD is the logical next step</li>

---

{{< slide background-image="/reveal-themes/scientist/text-5.jpg" transition="concave" auto-animate="">}}

# Helping Human Beings

<li>🌍 Bioinformatics is an efficient way to conduct biomedical research, and one of the best ways to influence living conditions globally</li>

---

{{< slide background-image="/reveal-themes/scientist/text-3.jpg" transition="concave">}}

# Working in an International Environment

<li>💫 I would like to continue improving my language skills, all while being in contact with people from different cultures and places, where diversity is always a plus</li>
</br>
<small>(But I'm not going to lie, I would also like to come back to Spain 😅)</small>

---

{{< slide background-image="/reveal-themes/scientist/text-1.jpg" transition="concave">}}

# Developing new skills

<li>💡 I really enjoy learning innovative skills (like <a href="https://revealjs.com/">reveal.js</a>! 😎), and I would like to work in a place where I can try many different things, feeding my curiosity</li>

{{% /section %}}

---

{{< slide background-image="/reveal-themes/scientist/end.jpg" data-transition="concave">}}

<div style="display: flex" class="row">

  <div style="flex: 60%" class="column">

  </br></br>

  # Thank you!

  Find more about me on [my website!](https://www.pablomarcos.me/)

  [↩️](#) Back to the beggining

  </br></br></br></br>

  </div>
  <div style="flex: 60%" class="column"></div>
</div>

<img style="position: absolute; right: 1000px; bottom: 0px;" data-src="/reveal-themes/creativecommons.png" width="20%">
