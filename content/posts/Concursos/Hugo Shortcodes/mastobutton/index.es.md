---
title: "MastoButton"
date: 2024-12-02
menu:
  sidebar:
    name: MastoButton
    identifier: mastobutton
    parent: hugo_shortcodes
    weight: 40
---

{{< alert type="warning" >}}
This project is a Fork of grayleonard/mastodon-share, which has been inactive since 2018
{{< /alert >}}

On December 2024, I tried to fix the mastobutton that has been part of this website's code for a long time. Since the original project did not accept contributions anymore (or, at least, it had pretty empty, 6-year-old PRs, one of which was even mine 🤣), I made some changes to it and forked it into my own repo, so that I could add it as a gitmodule.

An example of the three offered buttons is below:

<div class="mastobutton-container" style = "display: flex; justify-content: flex-start; gap: 10px;">
    {{< mastobutton >}}
    {{< mastobutton size="medium" >}}
    {{< mastobutton size="small" >}}
</div>

Contributions are welcome!

---



